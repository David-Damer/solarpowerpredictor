""" Linting script to run pylint on project and
    exit with fail or success code depending on
    the pylint score
"""
from pylint.lint import Run
SUCCESS = 0
FAIL = 1
OPTIONS = ['./smartergrid']

RESULT = Run(OPTIONS, do_exit=False)
SCORE = RESULT.linter.stats['global_note']
print(SCORE)
if SCORE >= 5.0:
    exit(SUCCESS)
else:
    print('Pylint score is too low')
    exit(FAIL)
