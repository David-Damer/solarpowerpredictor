### Solar Panel output Predictor
Django and JavaScript based application for predicting solar panel output given weather.

## Requirements 

MongoDB

Linux operating system 

Python 3

PIP

### Dependencies

All dependencies can be found in requirements.txt. To install run 

$ pip install -r requirements.txt

### Installation

The collected generator and weather data can be added to the database by running 

$ python generators_mongodb.py

and

$ python weatherDataMongoDB.py

### Running

In the top level of the directory, containing manage.py run

$ python manage.py runserver

and point your browser to http://127.0.0.1:8000/

### Testing

To run the tests, in the top level of the directory run. Ensure that the database is populated.


$ python manage.py test smartergrid

currently have 90% branch coverage

### Usage

##### Prediction

On the home page enter the date, generator and location you want the prediction for and click generate.

You can compare between 2 locations, dates and generators on this page.

##### Comparison

For analytics click compare and enter the date and location that you want to see a comparison for. A graph will be displayed showing, a plot of the reported power and energy output of the location and a plot of the energy predicted for that location.



