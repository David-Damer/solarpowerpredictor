

import numpy as np
import numpy.linalg as la


def julianDate(isoTime):

    # https://en.wikipedia.org/wiki/Julian_day#Julian_day_number_calculation

    year = float(isoTime[0:4])
    month = float(isoTime[5:7])
    day = float(isoTime[8:10])
    hour = float(isoTime[11:13])
    minute = float(isoTime[14:16])
    second = float(isoTime[17:19])

    return day - 32077.0 \
        + (1461.0 * (year + 4800.0 + (month - 14.0) // 12.0)) // 4.0 \
        + (367.0 * (month - 2.0 - 12.0 * ((month - 14.0) // 12.0))) // 12.0 \
        - (3.0 * ((year + 4900.0 + (month - 14.0) // 12.0) // 100.0)) // 4.0 \
        + (hour - 12.0) / 24.0 + minute / 1440.0 + second / 86400.0


def j2000Date(julianDate):

    # https://en.wikipedia.org/wiki/Equinox_(celestial_coordinates)#J2000.0

    return julianDate - 2451545.0


def solarVectors(j2000Dates):

    # https://en.wikipedia.org/wiki/Position_of_the_Sun#Ecliptic_coordinates

    meanLongitudes = 4.89495042 + 0.0172027923937 * j2000Dates
    meanAnomalies = 6.240040768 + 0.0172019703436 * j2000Dates

    meanLongitudes %= 2.0 * np.pi
    meanAnomalies %= 2.0 * np.pi

    eclipticLongitudes = meanLongitudes \
        + 0.033423055 * np.sin(meanAnomalies) \
        + 0.0003490659 * np.sin(2.0 * meanAnomalies)

    eclipticLatitudes = 0.0

    distances = 1.00014 \
        - 0.01671 * np.cos(meanAnomalies) \
        - 0.00014 * np.cos(2.0 * meanAnomalies)
    distances *= 149597870700

    # https://en.wikipedia.org/wiki/Position_of_the_Sun#Equatorial_coordinates

    meanAxialTilt = 0.409049500663

    rightAscensions = np.arctan2(np.cos(meanAxialTilt)
            * np.sin(eclipticLongitudes), np.cos(eclipticLongitudes))

    declinations = np.arcsin(np.sin(meanAxialTilt)
            * np.sin(eclipticLongitudes))

    return np.dstack((np.cos(rightAscensions) * np.cos(declinations),
        np.sin(rightAscensions) * np.cos(declinations),
        np.sin(declinations))) * distances


def positionVectors(j2000Dates, latitude, longitude, elevation):
    
    # https://aa.usno.navy.mil/faq/docs/GAST.php
    
    GMSTs = (4.89496121274 + 6.30038809898 * j2000Dates) % (2.0 * np.pi)
    
    longitudeAscending = 125.04 - 0.052954 * j2000Dates
    meanLongitude = 280.47 + 0.98565 * j2000Dates
    obliquity = 23.4393 - 0.0000004 * j2000Dates
    nutation = (-0.000319 * np.sin(longitudeAscending)
        - 0.000024 np.sin(2.0 * meanLongitude)) * np.cos(obliquity)
    
    GASTs = GMSTs + nutation
    LASTs = GASTs + longitude
    
    # https://en.wikipedia.org/wiki/Earth_radius#Location-dependent_radii
    
    a = 6378137.0
    b = 6356752.3
    radius = np.sqrt(
        ((a * a * np.cos(latitude)) ** 2 + (b * b * np.sin(latitude)) ** 2)
        / ((a * np.cos(latitude)) ** 2 + (b * np.sin(latitude)) ** 2))
    
    return np.dstack((np.cos(LASTs) * np.cos(latitude),
        np.sin(LASTs) * np.cos(latitude),
        np.sin(latitude))) * radius


def directionVectors(positions, orientation, tilt):
    
    #return np.array([np.sin(orientation) * np.sin(tilt),
    #    np.cos(orientation) * np.sin(tilt),
    #    np.cos(tilt)], dtype = np.float64)
    
    return la.norm(positions)

    
def calculateRatios(solars, positions, directions):

    directRatios = np.sum(la.norm(solars - positions) * directions, axis = 1)
    diffuseRatios = np.sum(la.norm(solars) * la.norm(positions), axis = 1)
    
    directRatios[np.where(directRatios < 0.0)] = 0.0
    diffuseRatios[np.where(diffuseRatios < 0.0)] = 0.0
    
    return directRatios, diffuseRatios
    

def readIrradianceFile(name, latitude, longitude):
    
    file = open(name, "r", encoding="ascii")
    
    ncols = int(file.readline().split()[1])
    nrows = int(file.readline().split()[1])
    xllcorner = float(file.readline().split()[1])
    yllcorner = float(file.readline().split()[1])
    cellsize = float(file.readline().split()[1])
    NODATA_value = float(file.readline().split()[1])
    
    x = (np.degrees(longitude) - xllcorner) / cellsize
    xf, xc = np.floor(x), np.ceil(x)
    y = (np.degrees(latitude) - yllcorner) / cellsize
    yf, yc = np.floor(y), np.ceil(y)
    
    i, v = 0, np.empty(4, dtype = np.float64)
    while i < nrows:
        l = f.readline()
        if i == int(yf):
            l1 = l.split()
            l2 = f.readline().split()
            v[0] = float(l1[int(xf)])
            v[1] = float(l1[int(xc)])
            if yf == yc:
                v[2] = v[0]
                v[3] = v[1]
            else:
                v[2] = float(l2[int(xf)])
                v[3] = float(l2[int(xc)])
            break
        i += 1
        
    if np.any(np.where(v == NODATA_value)):
        return 0.0
    
    return np.sum(v) / 4.0


def readIrradianceFiles(month, latitude, longitude):
    
    directValue, diffuseValue = 0.0, 0.0
    
    if directValue == 0.0:
        directValue = readfile("smartergrid/gh_2a_month_cmsaf/gh_2a_"
            + month + ".asc", latitude, longitude)
    
    if directValue == 0.0:
        directValue = readfile("smartergrid/gh_2a_month_sarah/gh_2a_"
            + month + ".asc", latitude, longitude)
    
    if diffuseValue == 0.0:
        diffuseValue = readfile("smartergrid/gh_0_month_cmsaf/gh_0_"
            + month + ".asc", latitude, longitude)
    
    if diffuseValue == 0.0:
        diffuseValue = readfile("smartergrid/gh_0_month_sarah/gh_0_"
            + month + ".asc", latitude, longitude)
    
    return directValue, diffuseValue


def extractWeatherData(dictionary, start, stop):
    
    array = np.empty((0, 7), dtype = np.float64)
    
    for k1 in dictionary.keys():
        for k2, val in dictionary[k1].items():
            if val is not None:
                time = j2000Date(julianDate(k1 + "T" + k2))
                if time >= start and time <= stop:
                    
                    temperature = weather[k1][k2]["curT"] - 273.15
                    humidity = weather[k1][k2]["humidity"] / 100.0
                    clouds = weather[k1][k2]["clouds"] / 100.0
                    pressure = weather[k1][k2]["pressure"] / 1000.0
                    windspeed = weather[k1][k2]["windspeed"]
                    rain = weather[k1][k2]["rain"]
                    
                    array = np.concatenate((array, [[
                        time,
                        temperature,
                        humidity,
                        clouds,
                        pressure,
                        windspeed,
                        rain, ]]))
                    
    return array[np.argsort(array[:,0])]


def calculate(samples, panel, installation, start, stop, latitude, longitude, elevation, orientation, tilt, weather):
    
    latitude = np.radians(latitude)
    longitude = np.radians(longitude)
    orientation = np.radians(orientation)
    tilt = np.radians(tilt)
    
    month = start[5:7]
    start = j2000Date(julianDate(start))
    stop = j2000Date(julianDate(stop))
    j2000Dates = np.linspace(start, stop, num = samples, dtype = np.float64)
    
    solars = solarVectors(j2000Dates)
    positions = positionVectors(j2000Dates, latitude, longitude, elevation)
    directions = directionVectors(positions, orientation, tilt)
        
    directRatios, diffuseRatios = calculateRatios(solars, positions, directions)
    directValue, diffuseValue = readIrradianceFiles(month, latitude, longitude)
    total = directRatios * directValue + diffuseRatios * diffuseValue
    
    weather = extractWeatherData(weather)
