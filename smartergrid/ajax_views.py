""" File for Handling Ajax Views
"""
import json
import math
import os.path
from datetime import datetime as dt

import numpy as np
from django.http import HttpResponseRedirect
from django.shortcuts import render
from pymongo import MongoClient

from smartergrid.calculate import calculate
from smartergrid_project.settings import BASE_DIR

LOCS_PATH = os.path.join(BASE_DIR, 'smartergrid', 'scripts', 'locs.txt')
START_TIME = 'T00:00:00'
END_TIME = 'T23:59:59'


def get_location_parameters(loc):
    """Function to read latitude, longitude and elevation from text
    file for location provided

    :param loc: name of location to get lat/lon for
    :type loc: str
    :return: latitude, longitude, elevation
    :rtype: (str, str, str)
    """
    lat = lon = ele = ngens = None
    try:
        with open(LOCS_PATH) as file:
            line = file.readline()
            while line:
                if line.split(',')[0] == loc:
                    lat = line.split(",")[1].rstrip().split(" ")[0]
                    lon = line.split(",")[1].rstrip().split(" ")[1]
                    ele = line.split(",")[2]
                    ngens = line.split(",")[3]
                line = file.readline()
        return lat, lon, ele, ngens
    except IOError:
        print('Error Reading from locs.txt')


def read_from_database(loc, date, gen, api):
    """Function to read data from database

    :param loc: location for weather data
    :type loc: str
    :param date: date to retrieve data for
    :type date: str
    :param gen: solar panel specs are required for
    :type gen: str
    :param api: api to use for weather data
    :type api: str
    :return: generator and weather data
    :rtype: (dict, dict)
    """
    cli = MongoClient('localhost', 27017)
    data_base = cli.smartergridDB
    weather_coll = data_base[api + loc.lower()]
    gen_coll = data_base.generators
    generator = gen_coll.find_one({'gen': gen})
    weather = weather_coll.find_one({}, {date})
    if weather is not None:
        del generator['_id']
        del weather['_id']

    return generator, weather


def call_calculate(date, gen, loc, api, data_points=1440, start_time=START_TIME, end_time=END_TIME):
    """ Call the calculate function """
    lat, lon, ele, ngens = get_location_parameters(loc)
    generator, weather = read_from_database(loc, date, gen, api)
    total, cumulative = calculate(data_points, generator, float(ngens), date + start_time,
                                  date + end_time,
                                  float(lat), float(lon), float(ele), 0.0, 0.0, weather)

    return cumulative, total


def get_graph_data(date, gen, loc, data_points=1440, start_time=START_TIME, end_time=END_TIME):
    """ get graph data to pass to chart.js """
    cumulative_owm, total_owm = call_calculate(date, gen, loc, 'owm', data_points, start_time,
                                               end_time)
    cumulative_yh, total_yh = call_calculate(date, gen, loc, 'yh', data_points, start_time,
                                             end_time)
    return cumulative_owm, cumulative_yh, total_owm, total_yh


def add_to_json_data(comp, cumulative_owm, cumulative_yh, json_data, total_owm, total_yh):
    """ adds to json_data """
    json_data['total_1_owm'] = list(total_owm)
    json_data['cumulative_owm'] = list(cumulative_owm)
    json_data['total_1_yh'] = list(total_yh)
    json_data['cumulative_yh'] = list(cumulative_yh)
    json_data['comp'] = comp


def graph(request):
    """ Function to render a graph
        using ajax  """

    date_c = None

    # If not a get request or trying to access page without required data
    # redirects to home page
    if request.method == 'GET':
        try:
            date = request.GET['day']
            gen = request.GET['gen']
            loc = request.GET['loc']
            comp = request.GET['comp']
            if int(comp) % 2 == 1:
                date_c = request.GET['dayC']
                gen_c = request.GET['genC']
                loc_c = request.GET['locC']
        except KeyError:
            return HttpResponseRedirect("/smartergrid/")

    else:
        print('Error: Not a GET request')
        return HttpResponseRedirect("/smartergrid/")

    error, check = read_from_database(loc, date, gen, 'yh')
    if check is None:
        json_data = None
        error = True
        context = {'json_data': json_data, 'error': error}
        return render(request, 'ajax_graph.html', context)

    error, check = read_from_database(loc, date, gen, 'owm')
    if check is None:
        json_data = None
        error = True
        context = {'json_data': json_data, 'error': error}
        return render(request, 'ajax_graph.html', context)

    cumulative_owm, cumulative_yh, total_owm, total_yh = get_graph_data(date, gen, loc)

    # if comparing 2 locations, generators etc.
    if date_c is not None:
        cumulative2_owm, cumulative2_yh, total2_owm, total2_yh = get_graph_data(date_c, gen_c,
                                                                                loc_c)
        json_data = {'loc': loc, 'gen': gen, 'date': date, 'locC': loc_c, 'genC': gen_c,
                     'dateC': date_c, 'total2_owm': list(total2_owm),
                     'cumulative2_owm': list(cumulative2_owm), 'total2_yh': list(total2_yh),
                     'cumulative2_yh': list(cumulative2_yh)}

    else:
        json_data = {'loc': loc, 'gen': gen, 'date': date}

    add_to_json_data(comp, cumulative_owm, cumulative_yh, json_data, total_owm, total_yh)
    error = False
    json_data = json.dumps(json_data)
    context = {'json_data': json_data, 'error': error}

    return render(request, 'ajax_graph.html', context)


def read_actual(loc, date):
    """ function to read and parse actual data

    :param loc: location for weather and generator
    :type loc: str
    :param date: date we need data for
    :type date: str
    :return the actual data as lists
    :rtype (list, list, list)
    """

    client = MongoClient('localhost', 27017)
    db = client.smartergridDB
    coll = db['actual' + loc.lower()]

    data = coll.find_one({}, {date})
    data = data[date]
    # read the actual data from json file and reverse lists to required format
    total = [float(j[2][:-1].replace(',', '')) for i, j in enumerate(data)][::-1]
    cumulative = [float(j[1][:-3].replace(',', '')) for i, j in enumerate(data) if j[1] != '-'][
                 ::-1]
    times = [dt.strptime(j[0], '%I:%M%p').time() for i, j in enumerate(data)][::-1]
    padding = [0.0 for i in range(len(total) - len(cumulative))]
    cumulative = padding + cumulative
    return total, cumulative, times


def compare_actual(request):
    """ function to render a graph to compare actual against predicted """

    if request.method == 'GET':
        try:
            date = request.GET['day']
            loc = request.GET['loc']
            gen = request.GET['gen']
        except KeyError:
            return HttpResponseRedirect("/smartergrid/")

    else:
        print('Error: Not a GET request')
        return HttpResponseRedirect("/smartergrid/")

    actual_total, actual_cumulative, actual_times = read_actual(loc, date)

    index_array = [i for i, j in enumerate(actual_total) if j > 0]  # list of non-zero elements
    start = index_array[0]  # first non-zero element
    end = index_array[-1]  # final non-zero element
    time_diff = actual_times[start + 1].minute - actual_times[start].minute  # time interval
    actual_times = [str(time) for time in actual_times]  # convert the times to strings
    offset_start = 60 // time_diff if start > (
                60 // time_diff) else 0  # offsets are extra time on each side of graph
    offset_end = 60 // time_diff if (end + (60 // time_diff)) < len(actual_times) else 0
    start_time = str(actual_times[start - offset_start])
    end_time = str(actual_times[end + offset_end])

    error, check = read_from_database(loc, date, gen, 'yh')
    if check is None:
        json_data = None
        error = True
        context = {'json_data': json_data, 'error': error}
        return render(request, 'compare.html', context)

    error, check = read_from_database(loc, date, gen, 'owm')
    if check is None:
        json_data = None
        error = True
        context = {'json_data': json_data, 'error': error}
        return render(request, 'compare.html', context)
    actual_total_range = actual_total[start - offset_start:end + offset_end]
    data_points = len(actual_total_range)
    cumulative_owm, cumulative_yh, total_owm, total_yh = get_graph_data(date, gen, loc, data_points,
                                                                        'T' + start_time,
                                                                        'T' + end_time)

    stddev_cum_owm = round(np.std(cumulative_owm), 4)
    CI95 = 1.96
    error_cum_owm = round(CI95 * stddev_cum_owm / math.sqrt(len(cumulative_owm)), 4)

    stddev_cumulative_yh = round(np.std(cumulative_yh), 4)
    error_cum_yh = round(CI95 * stddev_cumulative_yh / math.sqrt(len(cumulative_yh)), 4)

    total_power_owm = 0
    avrg_owm = 0
    max_owm = round(max(total_owm), 4)
    for t in total_owm:
        total_power_owm += t
    avrg_owm = round(total_power_owm / len(total_owm), 4)
    total_energy_owm = round(max(cumulative_owm), 4)
    total_power_yh = 0
    avrg_yh = 0
    max_yh = round(max(total_yh), 4)
    for t in total_yh:
        total_power_yh += t
    avrg_yh = round(total_power_yh / len(total_yh), 4)
    total_energy_yh = round(max(cumulative_yh), 4)
    total_power_actual = 0
    actual_cumulative_range = actual_cumulative[start - offset_start:end + offset_end]
    for i in actual_cumulative_range:
        total_power_actual += i
    avrg_actual = round(total_power_actual / len(actual_total_range), 4)
    max_actual = round(max(actual_total_range), 4)
    diff_energy_owm = round(max(actual_cumulative_range) - total_energy_owm,
                            4)
    diff_energy_yh = round(max(actual_cumulative_range) - total_energy_yh, 4)
    diff_max_owm = round(max(actual_total_range) - max_owm, 4)
    diff_max_yh = round(max(actual_total_range) - max_yh, 4)

    cumulative_owm, cumulative_yh, total_owm, total_yh = get_graph_data(date, gen, loc, data_points,
                                                                        'T' + start_time,
                                                                        'T' + end_time)

    actual_times_range = actual_times[start - offset_start:end + offset_end]
    json_comp = {'actual_total': actual_total_range,
                 'actual_cumulative': actual_cumulative_range,
                 'actual_times': actual_times_range,
                 'total_owm': list(total_owm), 'total_yh': list(total_yh),
                 'cumulative_owm': list(cumulative_owm),
                 'cumulative_yh': list(cumulative_yh), 'loc': loc, 'gen': gen, 'date': date,
                 'std_dev_owm': stddev_cum_owm, 'std_dev_yh': stddev_cumulative_yh,
                 'error_owm': error_cum_owm, 'error_yh': error_cum_yh,
                 'avrg_owm': avrg_owm, 'avrg_yh': avrg_yh,
                 'total_energy_owm': total_energy_owm, 'total_energy_yh': total_energy_yh,
                 'max_owm': max_owm, 'max_yh': max_yh,
                 'total_actual': max(actual_cumulative_range),
                 'avrg_actual': avrg_actual, 'max_actual': max_actual,
                 'diff_max_owm': diff_max_owm, 'diff_max_yh': diff_max_yh,
                 'diff_energy_owm': diff_energy_owm, 'diff_energy_yh': diff_energy_yh}
    json_compare = json.dumps(json_comp)
    error = False
    context = {'json_comp': json_comp, 'json_compare': json_compare, 'error': error}

    return render(request, 'compare.html', context)
