"""Populate the database with generators"""
from pymongo import MongoClient

try:
    CONN = MongoClient()
    print("Connected successfully!!!")
except:
    print("Could not connect to MongoDB")

# database
DB = CONN.smartergridDB
print(DB)
DB.generators.drop()
# Created or Switched to collection names: my_gfg_collection
COLLECTION = DB.generators

GEN1 = {"type": "admin", "gen": "TSM-DE05A-305", 'details':
    {'AREA': 1.6368,
     'STC': {"Isc": 9.85, "Voc": 40.0, "Mpp": 305.0, "Imp": 9.28, "Vmp": 32.9, "FF": 77.49},
     'NOCT': {"Isc": 7.95, "Voc": 37.2, "Mpp": 227.0, "Imp": 7.46, "Vmp": 30.5, "FF": 76.93},
     'TEMP': {"IscTemp": 0.05, "VocTemp": -0.29, "MppTemp": -0.39},
     'IRRADEFF': [17.08, 17.09, 17.12, 17.15, 17.21,
                  17.25, 17.51, 18.09, 18.53, 18.6], 'WARRANTEFF': [97, 97, 96.22, 95.44, 94.66,
                                                                    93.88, 93.1, 92.32, 91.54,
                                                                    90.76,
                                                                    90.0, 89.22, 88.45, 87.67,
                                                                    86.9, 86.12, 85.35, 84.57,
                                                                    83.8, 83.03, 82.26, 81.49,
                                                                    80.72, 79.95, 79.18, 78.41]}}

GEN2 = {"type": "admin", "gen": "JKM315M-60H", 'details': {"AREA": 1.687368,
                                                           "STC": {"Isc": 10.04, "Voc": 40.7,
                                                                   "Mpp": 315.0,
                                                                   "Imp": 9.49, "Vmp": 33.2,
                                                                   "FF": 77.10},
                                                           "NOCT": {"Isc": 8.33, "Voc": 37.6,
                                                                    "Mpp": 235.0,
                                                                    "Imp": 7.56, "Vmp": 31.2,
                                                                    "FF": 75.31},
                                                           "TEMP": {"IscTemp": 0.048,
                                                                    "VocTemp": -0.28,
                                                                    "MppTemp": -0.36},
                                                           "IRRADEFF": [17.71, 17.71, 17.78,
                                                                        18.51, 18.37,
                                                                        18.28, 18.33, 18.45,
                                                                        18.53, 18.67],
                                                           "WARRANTEFF": [97, 97, 96.328,
                                                                          95.656, 94.984,
                                                                          94.312, 93.64,
                                                                          92.968, 92.296,
                                                                          91.624,
                                                                          90.952, 90.28,
                                                                          90, 89.328,
                                                                          88.656,
                                                                          87.984, 87.312,
                                                                          86.64, 85.968,
                                                                          85.296,
                                                                          84.624, 83.952,
                                                                          83.28, 82.608,
                                                                          81.936,
                                                                          80.2]
                                                           }}

GEN3 = {"type": "admin", "gen": "JKM310M-60", 'details': {"AREA": 1.6368,
                                                          "STC": {"Isc": 9.92, "Voc": 40.5,
                                                                  "Mpp": 310.0, "Imp": 9.4,
                                                                  "Vmp": 33, "FF": 77.28},
                                                          "NOCT": {"Isc": 8.2, "Voc": 37.4,
                                                                   "Mpp": 231.0,
                                                                   "Imp": 7.49, "Vmp": 31,
                                                                   "FF": 76.45},
                                                          "TEMP": {"IscTemp": 0.048,
                                                                   "VocTemp": -0.28,
                                                                   "MppTemp": -0.37},
                                                          "IRRADEFF": [17.71, 17.72, 17.78,
                                                                       18.28, 18.37,
                                                                       18.32, 18.33, 18.72,
                                                                       18.53, 18.94],
                                                          "WARRANTEFF": [97, 97, 96.328,
                                                                         95.656, 94.984,
                                                                         94.312, 93.64,
                                                                         92.968, 92.296,
                                                                         91.624,
                                                                         90.952, 90.28,
                                                                         90, 89.328,
                                                                         88.656,
                                                                         87.984, 87.312,
                                                                         86.64, 85.968,
                                                                         85.296,
                                                                         84.624, 83.952,
                                                                         83.28, 82.608,
                                                                         81.936,
                                                                         80.2]
                                                          }}

GEN4 = {"type": "admin", "gen": "Q.Pro-G3-250", 'details': {"AREA": 1.67,
                                                            "STC": {"Isc": 8.71, "Voc": 37.49,
                                                                    "Mpp": 252.5,
                                                                    "Imp": 8.21, "Vmp": 30.76,
                                                                    "FF": 77.33},
                                                            "NOCT": {"Isc": 7.03, "Voc": 34.9,
                                                                     "Mpp": 186.0,
                                                                     "Imp": 6.44, "Vmp": 28.89,
                                                                     "FF": 75.81},
                                                            "TEMP": {"IscTemp": 0.04,
                                                                     "VocTemp": -0.30,
                                                                     "MppTemp": -0.42},
                                                            "IRRADEFF": [14.175, 14.625,
                                                                         14.85, 15, 15.15,
                                                                         15.15, 15.15, 15.1,
                                                                         15.05, 15],
                                                            "WARRANTEFF": [97, 97, 96.4,
                                                                           95.8, 95.2,
                                                                           94.6, 94, 93.4,
                                                                           92.8, 92.2,
                                                                           92, 91.4,
                                                                           90.8, 90.2,
                                                                           89.6,
                                                                           89, 88.4,
                                                                           87.8, 87.2,
                                                                           86.6,
                                                                           86, 85.4,
                                                                           84.8, 84.2,
                                                                           83.6,
                                                                           83]
                                                            }}

GEN5 = {"type": "admin", "gen": "Q.Peak-G4-1-290", 'details': {"AREA": 1.67,
                                                               "STC": {"Isc": 9.63, "Voc": 39.19,
                                                                       "Mpp": 290.0,
                                                                       "Imp": 9.07, "Vmp": 31.96,
                                                                       "FF": 76.84},
                                                               "NOCT": {"Isc": 7.76, "Voc": 36.87,
                                                                        "Mpp": 216.4,
                                                                        "Imp": 7.12, "Vmp": 30.39,
                                                                        "FF": 75.63},
                                                               "TEMP": {"IscTemp": 0.04,
                                                                        "VocTemp": -0.28,
                                                                        "MppTemp": -0.39},
                                                               "IRRADEFF": [16.008, 16.704,
                                                                            17.052, 17.45,
                                                                            17.45,
                                                                            17.5, 17.45,
                                                                            17.45, 17.42,
                                                                            17.4],
                                                               "WARRANTEFF": [98, 97.4,
                                                                              96.8, 96.2,
                                                                              95.6,
                                                                              95, 94.4,
                                                                              93.8, 93.2,
                                                                              92.6,
                                                                              92, 91.4,
                                                                              90.8, 90.2,
                                                                              89.6,
                                                                              89, 88.4,
                                                                              87.8, 87.2,
                                                                              86.6,
                                                                              86, 85.4,
                                                                              84.8, 84.2,
                                                                              83.6,
                                                                              83]
                                                               }}

GEN6 = {"type": "admin", "gen": "JC250M-24-Bb", 'details': {"AREA": 1.62688,
                                                            "STC": {"Isc": 8.83, "Voc": 37.4,
                                                                    "Mpp": 250.0,
                                                                    "Imp": 8.31, "Vmp": 30.1,
                                                                    "FF": 75.70},
                                                            "NOCT": {"Isc": 7.12, "Voc": 35,
                                                                     "Mpp": 185.0,
                                                                     "Imp": 6.57, "Vmp": 28.2,
                                                                     "FF": 74.24},
                                                            "TEMP": {"IscTemp": 0.04,
                                                                     "VocTemp": -0.3,
                                                                     "MppTemp": -0.4},
                                                            "IRRADEFF": [15.26, 15.8, 16.1,
                                                                         16.2, 16.2,
                                                                         16.2, 16.15, 16.1,
                                                                         16.05, 16.0],
                                                            "WARRANTEFF": [100, 97.5,
                                                                           96.8, 96.1,
                                                                           95.4,
                                                                           94.7, 94, 93.3,
                                                                           92.6, 91.9,
                                                                           91.2, 90.5,
                                                                           89.8, 89.1,
                                                                           88.4,
                                                                           87.7, 87,
                                                                           86.3, 85.6,
                                                                           84.9,
                                                                           84.2, 83.5,
                                                                           82.8, 82.1,
                                                                           81.4,
                                                                           80.7]
                                                            }}

GEN7 = {"type": "admin", "gen": "TSM-PE05H-285", 'details': {"AREA": 1.6616,
                                                             "STC": {"Isc": 9.45, "Voc": 38.8,
                                                                     "Mpp": 285.0,
                                                                     "Imp": 9.08, "Vmp": 31.4,
                                                                     "FF": 77.73},
                                                             "NOCT": {"Isc": 7.63, "Voc": 35.9,
                                                                      "Mpp": 211.0,
                                                                      "Imp": 7.18, "Vmp": 29.4,
                                                                      "FF": 77.03},
                                                             "TEMP": {"IscTemp": 0.05,
                                                                      "VocTemp": -0.32,
                                                                      "MppTemp": -0.41},
                                                             "IRRADEFF": [15.26, 15.8, 16.1,
                                                                          16.2, 16.2,
                                                                          16.2, 16.15, 16.1,
                                                                          16.05, 16.0],
                                                             "WARRANTEFF": [97.5, 97.5,
                                                                            96.8, 96.1,
                                                                            95.4,
                                                                            94.7, 94,
                                                                            93.3, 92.6,
                                                                            91.9,
                                                                            91.2, 90.5,
                                                                            89.8, 89.1,
                                                                            88.4,
                                                                            87.7, 87,
                                                                            86.3, 85.6,
                                                                            84.9,
                                                                            84.2, 83.5,
                                                                            82.8, 82.1,
                                                                            81.4,
                                                                            80.7]
                                                             }}
GEN8 = {"type": "admin", "gen": "ALM-250D-Rugby", 'details': {"AREA": 1.623,
                                                              "STC": {"Isc": 8.75, "Voc": 37.9,
                                                                      "Mpp": 250.0,
                                                                      "Imp": 7.99, "Vmp": 31.3,
                                                                      "FF": 75.39},
                                                              "NOCT": {"Isc": 7.12, "Voc": 35,
                                                                       "Mpp": 185.0,
                                                                       "Imp": 6.57, "Vmp": 28.2,
                                                                       "FF": 74.24},
                                                              "TEMP": {"IscTemp": 0.06,
                                                                       "VocTemp": -0.322,
                                                                       "MppTemp": -0.446},
                                                              "IRRADEFF": [16.3, 16.7, 16.8,
                                                                           17.1, 17.15,
                                                                           17.25, 17.25,
                                                                           17.25, 17.24,
                                                                           17.2],
                                                              "WARRANTEFF": [100.0, 99.2, 98.4,
                                                                             97.6, 96.8, 96.0, 95.2,
                                                                             94.4, 93.6, 92.8, 92.0,
                                                                             91.2, 90.4, 89.6, 88.8,
                                                                             88.0, 87.2, 86.4, 85.6,
                                                                             84.8, 84.0, 83.2, 82.4,
                                                                             81.6, 80.8, 80.0]
                                                              }}

GEN9 = {"type": "admin", "gen": "SW 285-Mono --Newquay", 'details': {"AREA": 1.474,
                                                                     "STC": {"Isc": 9.84,
                                                                             "Voc": 39.7,
                                                                             "Mpp": 285.0,
                                                                             "Imp": 9.20,
                                                                             "Vmp": 31.3,
                                                                             "FF": 72.96},
                                                                     "NOCT": {"Isc": 7.96,
                                                                              "Voc": 36.4,
                                                                              "Mpp": 213.1,
                                                                              "Imp": 7.43,
                                                                              "Vmp": 28.7,
                                                                              "FF": 73.55},
                                                                     "TEMP": {"IscTemp": 0.04,
                                                                              "VocTemp": -0.30,
                                                                              "MppTemp": -0.41},
                                                                     "IRRADEFF": [15.9, 16.21,
                                                                                  16.85,
                                                                                  17.0, 17.15,
                                                                                  17.25, 17.25,
                                                                                  17.25, 17.24,
                                                                                  17.2],
                                                                     "WARRANTEFF": [100.0, 99.3,
                                                                                    98.6, 97.9,
                                                                                    97.2, 96.5,
                                                                                    95.8, 95.1,
                                                                                    94.4, 93.7,
                                                                                    93.0, 92.3,
                                                                                    91.6, 90.9,
                                                                                    90.2, 89.5,
                                                                                    88.8, 88.1,
                                                                                    87.4, 86.7,
                                                                                    86.0, 85.3,
                                                                                    84.6, 83.9,
                                                                                    83.2, 82.5]
                                                                     }}
# Insert Data
REC_ID1 = COLLECTION.insert_one(GEN1)
REC_ID2 = COLLECTION.insert_one(GEN2)
REC_ID3 = COLLECTION.insert_one(GEN3)
REC_ID4 = COLLECTION.insert_one(GEN4)
REC_ID5 = COLLECTION.insert_one(GEN5)
REC_ID6 = COLLECTION.insert_one(GEN6)
REC_ID7 = COLLECTION.insert_one(GEN7)
REC_ID8 = COLLECTION.insert_one(GEN8)
REC_ID9 = COLLECTION.insert_one(GEN9)

print("Data inserted with record ids", REC_ID1, " ", REC_ID2, " ", REC_ID3, " ", REC_ID4, " ",
      REC_ID5, " ", REC_ID6, " ", REC_ID7)

# Printing the data inserted
CURSOR = COLLECTION.find()
for record in CURSOR:
    print(record)
