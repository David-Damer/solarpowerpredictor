""" This file handles scheduling api calls and saving the data
"""
from datetime import datetime as dt
import os.path
import json
from requests_oauthlib import OAuth1
import requests




API_FILES = ["owmKey.txt", "yhKey.txt", "yhOA.txt"]
API_ADDRESSES = ["http://api.openweathermap.org/data/2.5/weather?lat=REPLAT&lon=REPLON&APPID=REPKEY",
                 "https://weather-ydn-yql.media.yahoo.com/forecastrss?lat=LATREP&lon=LONREP&u=c&format=json"]


##populates the dictionary with api keys
def get_keys():
    key_dict = {}
    f_path = os.path.dirname(__file__)
    for f_name in API_FILES:
        filename = os.path.join(f_path, f_name)
        f_open = open(filename, "r")
        apk = f_open.readlines()[0]
        f_open.close()
        key_dict[f_name] = apk
    return(key_dict)

def get_locs():
    f_path = os.path.dirname(__file__)
    filename = os.path.join(f_path, "locs.txt")
    f_open = open(filename, "r")
    locs = f_open.readlines()
    f_open.close
    return(locs)


##checks if a file exists. if not create one
def check_file(f_name):
    f_path = os.path.dirname(__file__)
    fileloc = os.path.join(f_path, f_name)
    if not (os.path.isfile(fileloc)):
        with open(fileloc, "a") as f:
            f.write("{\n")
            f.write("}")

def convert_to_kelvin(in_temp):
    return (in_temp+273.15)

def convert_kmh_mps(in_speed):
    return (in_speed/3.6)

## returns the time in 24 hour time
def convert_pm_allday(in_time):
    return (in_time+12)

def save_data(f_name, weather_data):
    check_file(f_name)
    ##load the file and all its data into a dict
    fileloc = os.path.join(os.path.dirname(__file__), f_name)
    with open(fileloc, "r") as feedsjson:
        feeds = json.load(feedsjson)
    ##get the date and time for indexing the elements
    d = str(dt.now()).split(" ")[0]
    t = str(dt.now()).split(" ")[1].split(".")[0]
    ##if the date doesnt exist make and entry for it
    ##otherwise just add the weather data
    if d in feeds:
        feeds[d][t] = weather_data
    else:
        feeds[d] = {}
        feeds[d][t] = weather_data
    with open(fileloc, "w") as feedin:
        feedin.write(json.dumps(feeds, indent=2))

#makes a request to owm and will return a negative number if unsucessful
def owm_request(loc_str, key_dict):

    lat = loc_str.split(",")[1].split(" ")[0]
    lon = loc_str.split(",")[1].split(" ")[1].strip()
    q_string = API_ADDRESSES[0].replace("REPKEY", key_dict["owmKey.txt"]).replace("REPLAT", lat).replace("REPLON", lon)

    response = requests.get(q_string)
    if not(response.status_code == 200):
        return(-1)
    else:
        if response.text == None:
            return -2
        else:
            data = json.loads(response.text)
            weather_dat = {"curT":0, "humidity":0, "daylight":0, "clouds":0, "pressure":0, "windspeed":0, "rain":0}
            weather_dat["curT"] = data["main"]["temp"]
            weather_dat["humidity"] = data["main"]["humidity"]
            weather_dat["clouds"] = data["clouds"]["all"]
            weather_dat["daylight"] = ((data["sys"]["sunset"] - data["sys"]["sunrise"]) / 60)/60
            weather_dat["pressure"] = data["main"]["pressure"]
            weather_dat["windspeed"] = data["wind"]["speed"]
        
            location = loc_str.split(",")[0]
            f_name = "data/owm" + location + ".json"
            save_data(f_name,weather_dat)


            return(1)


#makes a request to yahoo api and will return a negatve number if unsucessful
def yahoo_request(loc_str, key_dict):
    lat = loc_str.split(",")[1].split(" ")[0]
    lon = loc_str.split(",")[1].split(" ")[1].strip()
    q_string = API_ADDRESSES[1].replace("LATREP", lat).replace("LONREP", lon)
    auth = OAuth1(key_dict["yhOA.txt"], key_dict["yhKey.txt"])
    response = requests.get(q_string, auth=auth)
    if(response.status_code != 200):
        print(response.status_code)
        return(-1)
    else:
        ##SAVE DATA AFTER THIS
        f_name = loc_str.split(",")[0]
        f_name = "data/yh" + f_name + ".json"
        data = json.loads(response.text)
        if len(data["current_observation"]) == 0:
            return -2
        else:
            weather_dat = {"curT":0, "humidity":0, "clouds":0, "pressure":0, "windspeed":0}
            weather_dat["pressure"] = data["current_observation"]["atmosphere"]["pressure"]
            weather_dat["humidity"] = data["current_observation"]["atmosphere"]["humidity"]
            weather_dat["windspeed"] = convert_kmh_mps(data["current_observation"]["wind"]["speed"])
            weather_dat["curT"] = convert_to_kelvin(int(data["current_observation"]["condition"]["temperature"]))
            cond = data["current_observation"]["condition"]["text"]
            if cond == "Cloudy":
                weather_dat["clouds"] = 40
            elif cond == "Mostly Cloudy":
                weather_dat["clouds"] = 75
            elif cond == "Partly Cloudy":
                weather_dat["clouds"] = 20
            else:
                weather_dat["clouds"] = 0
            location = loc_str.split(",")[0]
            f_name = "data/yh" + location + ".json"
            save_data(f_name,weather_dat)
   

        return(1)


def get_sunrise(key_dict):
        auth = OAuth1(key_dict["yhOA.txt"], key_dict["yhKey.txt"])
        q_string = API_ADDRESSES[1].replace("LATREP", "55.8642").replace("LONREP", "-4.2518")
        response = requests.get(q_string,auth = auth)
        if(response.status_code == 200):
                data = json.loads(response.text)
                return(data["current_observation"]["astronomy"]["sunrise"].split(":")[0])

def get_sunset(key_dict):
        auth = OAuth1(key_dict["yhOA.txt"], key_dict["yhKey.txt"])
        q_string = API_ADDRESSES[1].replace("LATREP", "55.8642").replace("LONREP", "-4.2518")
        response = requests.get(q_string,auth = auth)
        if(response.status_code == 200):
                data = json.loads(response.text)
                return(data["current_observation"]["astronomy"]["sunset"].split(":")[0])


def scheduler(key_dict, locs):
    for loc in locs:
        ##returns -1 if the request fails and -2 if the data is malformed
        ## otherwise should return 1
        owm_request(loc, key_dict)
        yahoo_request(loc, key_dict)

def checkTime():
    cur_hour = int(str(dt.now().time()).split(":")[0])
    ##CHECK IF ITS ONE HOUR BEFORE SUNRISE HERE
    key_dict = get_keys()
    print(key_dict)
    sr = int(get_sunrise(key_dict))
    ss = convert_pm_allday(int(get_sunset(key_dict)))
    #give two hours eaither side of the sunlight gap
    if(cur_hour>(sr-2) and cur_hour<(ss+2)):
        locs = get_locs()
        scheduler(key_dict,locs)



checkTime()
