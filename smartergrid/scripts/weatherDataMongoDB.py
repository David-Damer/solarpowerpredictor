import json
import os

from pymongo import MongoClient

client = MongoClient('localhost', 27017)
database = client['smartergridDB']
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
path = os.path.join(BASE_DIR, 'scripts', 'data', '')
names = os.listdir(path)
for name in names:
    s = name.rsplit(".", 1)
    if len(s) > 1:
        if s[1] == "json":
            with open(os.path.join(path, name)) as f:
                data = json.load(f)
                location = database[s[0].lower()]
                if data is not None:
                    location.drop()
                    location.insert(data)
                    print(path + name + " uploaded as " + s[0].lower())

client.close()
