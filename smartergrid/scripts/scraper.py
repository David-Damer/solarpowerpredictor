""" script to scrape data from pvoutput.org """
from datetime import datetime as dt
from os import path
import os
import json
import pandas as pd

BASE = path.dirname(path.dirname(path.abspath(__file__)))
HISTORICAL_DATA = path.join(BASE, 'scripts')

def check_file(f_name):
    """Function for checking if a file alreadty exists"""
    file_loc = os.path.join(BASE, f_name)
    if not os.path.isfile(file_loc):
        with open(file_loc, "a") as f:
            f.write("{\n")
            f.write("}")
    else:
        print ("file exists")

class Scraper:
    """ Class to scrape PvOutput """

    def __init__(self, urls):
        self.urls = urls
        self.save_data = {}

    def save_today(self):
        """save the data collected for today"""
        for loc in  self.save_data:
            file_loc = HISTORICAL_DATA+ "/data/" +loc+ ".json"
            check_file(file_loc)

            ##Load in file
            with open(file_loc, "r") as feedsjson:
                feeds = json.load(feedsjson)
            ##Update file

            feeds[str(dt.now().date())] = self.save_data[loc]

            ##Resave file
            with open(file_loc, "w") as feedin:
                feedin.write(json.dumps(feeds, indent=2))

    def get_today(self):
        """ Collect todays data"""
        today = dt.now().date()
        today_str = today.strftime('%Y%m%d')
        for url in self.urls:
            tables = pd.read_html(
                self.urls[url] + today_str)
            time = tables[0]["Time"]
            energy = tables[0]["Energy"]
            power = tables[0]["Power"]
            self.save_data[url] = list(zip(time, energy, power))[0:-1]



if __name__ == "__main__":
    SCRAPER = Scraper({"ActualTestPanelLoc":"https://pvoutput.org/intraday.jsp?id=6438&sid=26965&dt=",
                       "ActualNewquayGen":"https://pvoutput.org/intraday.jsp?id=66660&sid=59275&dt=",
                       "ActualRugbyGen":"https://pvoutput.org/intraday.jsp?id=8191&sid=6633&dt="})
    SCRAPER.get_today()
    SCRAPER.save_today()
