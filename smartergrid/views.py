""" Django Views file
"""

import datetime

from django.shortcuts import render
from pymongo import MongoClient


def index(request):
    yesterday = datetime.datetime.now().date() - datetime.timedelta(days=1)
    yesterday = str(yesterday)
    client = MongoClient()
    db = client['smartergridDB']
    generators = db['generators']
    gens = []
    for gen in generators.find():
        gens.append(gen["gen"])
    cities = []
    filepath = 'smartergrid/scripts/locs.txt'
    with open(filepath) as fp:
        line = fp.readline()
        while line:
            a = line.split(",", 1)[0]
            cities.append(a)
            line = fp.readline()
    context_dict = {"generators": gens, "cities": cities, 'yesterday': yesterday}
    return render(request, 'pages/index.html', context_dict)

def actual(request):
    yesterday = datetime.datetime.now().date() - datetime.timedelta(days=1)
    yesterday = str(yesterday)
    client = MongoClient()
    db = client['smartergridDB']
    generators = db['generators']
    cities = []
    filepath = 'smartergrid/scripts/locs.txt'
    with open(filepath) as fp:
        line = fp.readline()
        while line:
            a = line.split(",", 1)[0]
            cities.append(a)
            line = fp.readline()
    gens = []
    for g in generators.find():
        if g["gen"] == "Q.Pro-G3-250":
            gen = [g["gen"], "testPanelLocation"]
            gens.append(gen)
        if g["gen"] == "ALM-250D-Rugby":
            gen = [g["gen"], "Rugby"]
            gens.append(gen)
        if g["gen"] == "SW 285-Mono --Newquay":
            gen = [g["gen"], "Newquay"]
            gens.append(gen)
    for gen in gens:
        print(gen[0])
    context_dict = {"generators": gens, 'yesterday': yesterday}
    return render(request, 'pages/actual.html', context_dict)


def new(request):
    context_dict = {}
    return render(request, 'pages/new.html', context_dict)


def panels(request):
    client = MongoClient()
    db = client['smartergridDB']
    generators = db['generators']
    gens = []
    valid = True
    for g in generators.find():
        gens.append(g)
    context_dict = {"generators": gens, "valid": valid}
    return render(request, 'pages/panels.html', context_dict)


def update(request, gen):
    client = MongoClient()
    db = client['smartergridDB']
    generators = db['generators']
    gens = []
    for g in generators.find():
        gens.append(g)
    context_dict = {"generators": gens, "gen": gen}
    return render(request, 'pages/update.html', context_dict)


#add new solar panels to database
def addNew(request):
    client = MongoClient()
    db = client['smartergridDB']
    generators = db['generators']
    name, area, stc, noct, temp, irradeff, warranteff = getRequest(request)
    valid = valitateInputs(name, area, stc, noct, temp, irradeff, warranteff)
    gens = []
    #check if the panel already exist
    for gen in generators.find():
        gens.append(gen)
    for gen in generators.find():
        if gen["gen"] == name:
            context_dict = {"generators": gens}
            return render(request, 'pages/panels.html', context_dict)
    #create panel
    GEN = {"type": "custom", "gen": name, 'details': {"AREA": float(area), "STC": stc, "NOCT": noct, "TEMP": temp, "IRRADEFF": irradeff, "WARRANTEFF": warranteff}}
    #add to database
    if valid:
        rec = generators.insert_one(GEN)
    gens = []
    for gen in generators.find():
        gens.append(gen)
    context_dict = {"generators": gens, "valid": valid}
    return render(request, 'pages/panels.html', context_dict)


#update current solar panels
def updateSolar(request):
    client = MongoClient()
    db = client['smartergridDB']
    generators = db['generators']
    oldName = request.GET['oldName']
    name, area, stc, noct, temp, irradeff, warranteff = getRequest(request)
    valid = valitateInputs(name, area, stc, noct, temp, irradeff, warranteff)
    #create panel
    GEN = {"type": "custom", "gen": name, 'details': {"AREA": float(area), "STC": stc, "NOCT": noct, "TEMP": temp, "IRRADEFF": irradeff, "WARRANTEFF": warranteff}}
    #remove old and add new panel to database
    if valid:
        db.generators.remove({"gen": oldName})
        rec = generators.insert_one(GEN)
    gens = []
    for gen in generators.find():
        gens.append(gen)
    context_dict = {"generators": gens, "valid": valid}
    return render(request, 'pages/panels.html', context_dict)


# get values from html
def getRequest(request):
    if request.method == 'GET':
        name = request.GET['name']
        area = request.GET['area']
        stc = getStc(request)
        noct = getNoct(request)
        temp = getTemp(request)
        irradeff = getIrradeff(request)
        warranteff = getWarranteff(request)
        return name, area, stc, noct, temp, irradeff, warranteff


def valitateInputs(name, area, stc, noct, temp, irradeff, warranteff):
    valid = True
    values = [irradeff, warranteff]
    dictio = [stc, noct]
    from string import punctuation
    invalid = set(punctuation)
    invalid.remove(".")
    invalid.remove("-")
    if any(char in invalid for char in name):
         valid = False
    if any(char in invalid for char in area):
         valid = False
    for v in values:
        if any(num in invalid for num in v):
            valid = False
    for d in dictio:
        for char in invalid:
            if char in str(d['Isc']):
                valid = False
            if char in str(d['Voc']):
                valid = False
            if char in str(d['Mpp']):
                valid = False
            if char in str(d['Imp']):
                valid = False
            if char in str(d['Vmp']):
                valid = False
            if char in str(d['FF']):
                valid = False
    for char in invalid:
        if char in str(temp['IscTemp']):
            valid = False
        if char in str(temp['VocTemp']):
            valid = False
        if char in str(temp['MppTemp']):
            valid = False
    return valid


# get all values for stc
def getStc(request):
    Sisc = request.GET['Sisc']
    Svoc = request.GET['Svoc']
    Smpp = request.GET['Smpp']
    Simp = request.GET['Simp']
    Svmp = request.GET['Svmp']
    Sff = request.GET['Sff']
    stc = {"Isc": float(Sisc), "Voc": float(Svoc), "Mpp": float(Smpp), "Imp": float(Simp), "Vmp": float(Svmp), "FF": float(Sff)}
    return stc


#get all values for noct
def getNoct(request):
    Nisc = request.GET['Nisc']
    Nvoc = request.GET['Nvoc']
    Nmpp = request.GET['Nmpp']
    Nimp = request.GET['Nimp']
    Nvmp = request.GET['Nvmp']
    Nff = request.GET['Nff']
    noct = {"Isc": float(Nisc), "Voc": float(Nvoc), "Mpp": float(Nmpp), "Imp": float(Nimp), "Vmp": float(Nvmp), "FF": float(Nff)}
    return noct


#get all values for temp
def getTemp(request):
    IscTemp = request.GET['iscTemp']
    VocTemp = request.GET['vocTemp']
    MppTemp = request.GET['mppTemp']
    temp = {"IscTemp": float(IscTemp), "VocTemp": float(VocTemp), "MppTemp": float(MppTemp)}
    return temp


#get all the values for irradeff
def getIrradeff(request):
    watts = 100
    wattsIncrease = 100
    k = 0
    irradeff = [None] * 10
    while watts in range(1100):
        irradeff[k] = request.GET[str(watts) + 'w']
        irradeff[k] = float(irradeff[k])
        k = k + 1
        watts = watts + wattsIncrease
    return irradeff


#get the 3 values for warrant from html and calculate the rest
def getWarranteff(request):
    warranteff = [None] * 26
    warranteff[0] = request.GET['1y']
    warranteff[9] = request.GET['10y']
    warranteff[24] = request.GET['25y']
    if warranteff[9] == "":
        eff = (float(warranteff[0]) - float(warranteff[24])) / 25
    else:
        eff = (float(warranteff[0]) - float(warranteff[9])) / 10
    i = 1
    years = 26
    while i in range(years):
        warranteff[i] = float(warranteff[0]) - eff * i
        i = i + 1
    warranteff[0] = float(warranteff[0])
    return warranteff


#delete existing panels
def deleteSolar(request, gen):
    client = MongoClient()
    db = client['smartergridDB']
    generators = db['generators']
    for g in generators.find():
        if gen == g["gen"]:
            db.generators.remove({"gen": gen})
    gens = []
    for gen in generators.find():
        gens.append(gen)
    context_dict = {"generators": gens}
    return render(request, 'pages/panels.html', context_dict)

