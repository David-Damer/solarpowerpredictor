import numpy as np
import scipy.integrate as ig
import scipy.interpolate as ip


def julianDate(isoDateTime):
    year = float(isoDateTime[0:4])
    month = float(isoDateTime[5:7])
    day = float(isoDateTime[8:10])
    hour = float(isoDateTime[11:13])
    minute = float(isoDateTime[14:16])
    second = float(isoDateTime[17:19])

    # https://en.wikipedia.org/wiki/Julian_day#Julian_day_number_calculation
    return day - 32077.0 \
           + (1461.0 * (year + 4800.0 + (month - 14.0) // 12.0)) // 4.0 \
           + (367.0 * (month - 2.0 - 12.0 * ((month - 14.0) // 12.0))) // 12.0 \
           - (3.0 * ((year + 4900.0 + (month - 14.0) // 12.0) // 100.0)) // 4.0 \
           + (hour - 12.0) / 24.0 + minute / 1440.0 + second / 86400.0


def generateJulianDates(samples, start, stop):
    # https://en.wikipedia.org/wiki/Equinox_(celestial_coordinates)#J2000.0
    start = julianDate(start) - 2451545.0
    stop = julianDate(stop) - 2451545.0

    return np.linspace(start, stop, num=samples)


def sunVector(julianDate, longitude, latitude):
    # https://en.wikipedia.org/wiki/Position_of_the_Sun#Ecliptic_coordinates
    meanLongitude = (4.89495042 + 0.0172027923937 * julianDate) \
                    % (2.0 * np.pi)
    meanAnomaly = (6.240040768 + 0.0172019703436 * julianDate) \
                  % (2.0 * np.pi)
    eclipticLongitude = meanLongitude \
                        + 0.033423055 * np.sin(meanAnomaly) \
                        + 0.0003490659 * np.sin(2.0 * meanAnomaly)

    # https://en.wikipedia.org/wiki/Position_of_the_Sun#Equatorial_coordinates
    declination = np.arcsin(np.sin(0.409049500663) \
                            * np.sin(eclipticLongitude))

    # https://en.wikipedia.org/wiki/Hour_angle#Solar_hour_angle
    hourAngle = np.fmod(julianDate, 1.0) * 2.0 * np.pi + longitude

    # https://en.wikipedia.org/wiki/Solar_zenith_angle
    zenithAngle = np.arccos(np.sin(latitude) * np.sin(declination) \
                            + np.cos(latitude) * np.cos(declination) * np.cos(hourAngle))

    # https://en.wikipedia.org/wiki/Solar_azimuth_angle
    azimuthAngle = np.arcsin((-1.0 * np.sin(hourAngle) \
                              * np.cos(declination)) / np.sin(zenithAngle))

    return np.dstack((np.sin(azimuthAngle) * np.sin(zenithAngle),
                      np.cos(azimuthAngle) * np.sin(zenithAngle), np.cos(zenithAngle)))


def panelVector(orientation, tilt):
    return np.dstack((np.sin(orientation) * np.sin(tilt),
                      np.cos(orientation) * np.sin(tilt), np.cos(tilt)))


def zenithVector():
    return np.dstack((0.0, 0.0, 1.0))


def cosineSimilarity(julianDate, longitude, latitude, orientation, tilt):
    s = sunVector(julianDate, longitude, latitude)

    direct = np.sum(s * panelVector(orientation, tilt), axis=2)
    diffuse = np.sum(s * zenithVector(), axis=2)

    direct[np.where((direct < 0.0) | (diffuse < 0.0))] = 0.0
    diffuse[np.where(diffuse < 0.0)] = 0.0

    return direct[0], diffuse[0]


def readRadiationFile(name, latitude, longitude): # pragma: no cover
    file = open(name, "r", encoding="ascii")

    ncols = int(file.readline().split()[1])
    nrows = int(file.readline().split()[1])
    xllcorner = float(file.readline().split()[1])
    yllcorner = float(file.readline().split()[1])
    cellsize = float(file.readline().split()[1])
    NODATAvalue = float(file.readline().split()[1])

    x = (np.degrees(longitude) - xllcorner) / cellsize
    xf, xc = np.floor(x), np.ceil(x)
    y = (np.degrees(latitude) - yllcorner) / cellsize
    yf, yc = np.floor(y), np.ceil(y)

    i, v = 0, np.empty(4)
    while i < nrows:
        l = file.readline()
        if i == int(yf):
            l1 = l.split()
            l2 = file.readline().split()
            v[0] = float(l1[int(xf)])
            v[1] = float(l1[int(xc)])
            if yf == yc:
                v[2] = v[0]
                v[3] = v[1]
            else:
                v[2] = float(l2[int(xf)])
                v[3] = float(l2[int(xc)])
            break
        i += 1

    if np.any(np.where(v == NODATAvalue)):
        return -1.0

    sqrt2 = np.sqrt(2.0)
    total = v[0] * np.sqrt((xc - x) ** 2 + (yc - y) ** 2) / sqrt2
    total += v[1] * np.sqrt((x - xf) ** 2 + (yc - y) ** 2) / sqrt2
    total += v[2] * np.sqrt((xc - x) ** 2 + (y - yf) ** 2) / sqrt2
    total += v[3] * np.sqrt((x - xf) ** 2 + (y - yf) ** 2) / sqrt2

    return total


def readRadiationFiles(month, latitude, longitude):  # pragma: no cover
    direct, diffuse = -1.0, -1.0

    if direct == -1.0:
        direct = readRadiationFile("smartergrid/gh_2a_month_cmsaf/gh_2a_"
                                   + month + ".asc", latitude, longitude)

    if direct == -1.0:
        direct = readRadiationFile("smartergrid/gh_2a_month_sarah/gh_2a_"
                                   + month + ".asc", latitude, longitude)

    if direct == -1.0:
        direct = 0.0

    if diffuse == -1.0:
        diffuse = readRadiationFile("smartergrid/gh_0_month_cmsaf/gh_0_"
                                    + month + ".asc", latitude, longitude)

    if diffuse == -1.0:
        diffuse = readRadiationFile("smartergrid/gh_0_month_sarah/gh_0_"
                                    + month + ".asc", latitude, longitude)

    if diffuse == -1.0:
        diffuse = 0.0

    return direct, diffuse


def extractWeatherData(weather, start, stop):
    start = julianDate(start)
    stop = julianDate(stop)

    array = np.empty((0, 7), dtype=np.float64)

    for k1 in weather.keys():
        for k2, val in weather[k1].items():
            if val is not None:
                time = julianDate(k1 + "T" + k2)
                if start <= time and time <= stop:

                    temperature = weather[k1][k2]["curT"] + 273.15
                    humidity = weather[k1][k2]["humidity"] / 100.0
                    clouds = weather[k1][k2]["clouds"] / 100.0
                    pressure = weather[k1][k2]["pressure"] / 1000.0
                    windspeed = weather[k1][k2]["windspeed"]
                    try:
                        rain = weather[k1][k2]["rain"]
                    except KeyError:
                        rain = 0
                    array = np.concatenate((array, [[
                        time - 2451545.0,
                        temperature,
                        humidity,
                        clouds,
                        pressure,
                        windspeed,
                        rain,
                    ]]))

    return array[np.argsort(array[:, 0])]


def solarFlux(julianDate, longitude, latitude):
    # https://en.wikipedia.org/wiki/Position_of_the_Sun#Ecliptic_coordinates
    meanLongitude = (4.89495042 + 0.0172027923937 * julianDate) \
                    % (2.0 * np.pi)
    meanAnomaly = (6.240040768 + 0.0172019703436 * julianDate) \
                  % (2.0 * np.pi)
    eclipticLongitude = meanLongitude \
                        + 0.033423055 * np.sin(meanAnomaly) \
                        + 0.0003490659 * np.sin(2.0 * meanAnomaly)
    distance = 1.00014

    # https://en.wikipedia.org/wiki/Position_of_the_Sun#Equatorial_coordinates
    declination = np.arcsin(np.sin(0.409049500663) \
                            * np.sin(eclipticLongitude))

    # https://en.wikipedia.org/wiki/Hour_angle#Solar_hour_angle
    hourAngle = np.fmod(julianDate, 1.0) * 2.0 * np.pi + longitude

    # https://en.wikipedia.org/wiki/Solar_zenith_angle
    elevationAngle = np.arcsin(np.sin(latitude) * np.sin(declination) \
                               + np.cos(latitude) * np.cos(declination) * np.cos(hourAngle))

    # https://en.wikipedia.org/wiki/Julian_year_(astronomy)
    j = 1 + julianDate % 365.25

    # https://www.sciencedirect.com/science/article/pii/S221260901400051X
    solarConstant = 1367.0
    correction = 1.0 + 0.034 * np.cos(j - 2.0)
    turbidity = 3.564 - 0.01 * np.sin(0.986 * (j + 284.0))
    directFlux = solarConstant * correction * turbidity \
                 * np.exp(-0.13 / np.sin(elevationAngle)) * np.sin(elevationAngle)
    diffuseFlux = 120.0 * turbidity \
                  * np.exp(-1.0 / (0.4511 * np.sin(elevationAngle)))

    directFlux[np.where(directFlux < 0.0)] = 0.0
    diffuseFlux[np.where(diffuseFlux < 0.0)] = 0.0

    return directFlux, diffuseFlux


def panelTotal(total, julianDates, weather, age, panel):
    total *= panel["details"]["AREA"]

    irradianceEfficiencies = np.array(panel["details"]["IRRADEFF"])
    irradiances = np.linspace(100.0, 1000.0, num=10)
    irradianceEfficiency = \
        ip.splev(total, ip.splrep(irradiances, irradianceEfficiencies))

    warrantyEfficiencies = np.array(panel["details"]["WARRANTEFF"])
    warranties = np.linspace(0.0, 25.0, num=26)
    warrantyEfficiency = \
        ip.splev(age, ip.splrep(warranties, warrantyEfficiencies))

    total *= (irradianceEfficiency * warrantyEfficiency) / 10000.0

    temperatureCorrection = \
        panel["details"]["TEMP"]["MppTemp"] * weather[:, 1] / 1000.0
    total += total * \
             ip.splev(julianDates, ip.splrep(weather[:, 0], temperatureCorrection))

    maximum = panel["details"]["STC"]["Imp"] * panel["details"]["STC"]["Vmp"]
    total[np.where(total > maximum)] = maximum
    total[np.where(total < 0.0)] = 0.0

    return total


def calculate(samples, panel, npanels, start, stop,
              latitude, longitude, elevation, orientation, tilt, weather):
    latitude = np.radians(latitude)
    longitude = np.radians(longitude)
    orientation = np.radians(orientation)
    tilt = np.radians(tilt)

    julianDates = generateJulianDates(samples, start, stop)  # - (1.0/12.0)

    directSimilarity, diffuseSimilarity = cosineSimilarity(julianDates,
                                                           longitude, latitude, orientation, tilt)
    directFlux, diffuseFlux = solarFlux(julianDates, longitude, latitude)
    total = directSimilarity * directFlux + diffuseSimilarity * diffuseFlux

    w = extractWeatherData(weather, start, stop)
    total *= (1 - 0.446369 * ip.splev(julianDates, ip.splrep(w[:, 0], w[:, 2])))
    total *= (1 - 0.469450 * ip.splev(julianDates, ip.splrep(w[:, 0], w[:, 3])))

    total = panelTotal(total, julianDates, w, 0.0, panel) * npanels
    total = np.nan_to_num(total)
    cumulative = np.append([0.0], ig.cumtrapz(total, julianDates * 24.0)) / 1000.0

    return total, cumulative
