from django.urls import path

from . import ajax_views
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('new', views.new, name='new'),
    path('update/(<gen>)', views.update, name='update'),
    path('actual', views.actual, name='actual'),
    path('panels', views.panels, name='panels'),
    path('smartergrid/compare/', ajax_views.compare_actual, name='compare_actual'),
    path('smartergrid/graph/', ajax_views.graph, name='graph'),
    path('addNew', views.addNew, name='addNew'),
    path('updateSolar', views.updateSolar, name='updateSolar'),
    path('deleteSolar/(<gen>)', views.deleteSolar, name='deleteSolar'),
]
