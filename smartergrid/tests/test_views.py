""" Tests for functions in Ajax_views and views
"""
from django.test import TestCase
from pymongo import MongoClient

from smartergrid.views import valitateInputs


class TestIndexView(TestCase):
    """ Class for testing the index page view """

    def test_generators_displayed(self):
        """ Test index page gets passed the generators
            correctly
        """
        print("Testing index page view (generators)")

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertIsNotNone(response.context['generators'])

    def test_cities_displayed(self):
        """ Test cities list gets passed
            correctly
        """
        print("Testing index page view (cities)")

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertEqual(response.context['cities'],
                         ['Glasgow', 'London', 'Harris', 'Cardiff', 'Brighton', 'Liverpool',
                          'Aberdeen', 'Inverness',
                          'Edinburgh', 'Dundee', 'Islay', 'Fort William', 'testPanelLocation',
                          'Newquay', 'Rugby'], )

    def test_date_on_page(self):
        """ Test that date is displayed on
            index page
        """
        print("Testing index page view (dates)")

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertContains(response, 'Date')


class TestPanelView(TestCase):
    """ Class for testing the panel view"""

    def test_panels_contains_panels(self):
        """ Test that the panels are fetched in the panels view"""

        print("Testing panel page (panels)")

        response = self.client.get('/smartergrid/panels')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertIsNot(response.context['generators'],
                         None)

    def test_panels_valid_is_true(self):
        """ Test valid is true"""
        print("Testing panel page (valid == true)")
        response = self.client.get('/smartergrid/panels')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertEqual(response.context['valid'], True)


class TestUpdateView(TestCase):
    """ Class to test update view"""

    def test_update_contains_generator(self):
        """ Test update displays generators """
        print("Testing update displays generators")

        response = self.client.get('/smartergrid/update/(TSM-DE05A-305)')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertIsNot(response.context['generators'],
                         None)


class TestNew(TestCase):
    """ Class to test new view """

    def test_context_has_correct_text(self):
        """ Testing context is empty for the new view"""
        print('Testing new displays correct text')

        response = self.client.get('/smartergrid/new')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertContains(response, 'Add New Solar Panel')


class TestAddNew(TestCase):
    """ Class to test add new page """

    @classmethod
    def tearDownClass(cls):
        """ Remove generators added by the tests """
        print('Removing generator from database')
        client = MongoClient()
        database = client['smartergridDB']
        database.generators.delete_one({'gen': 'Our generator'})
        database.generators.delete_one({'gen': 'Another generator'})
        client.close()

    def test_add_new_valid_with_adding_generator(self):
        """ Test that adding a generator works """
        print('Test adding a new generator')
        response = self.client.get(
            '/smartergrid/addNew?csrfmiddlewaretoken'
            '=9OeDCigl0J8kbsJCy6p4mCqtK4H7FhmtZFDC2nqCerkTGtfcDAKmxvgeJtGGKfXq&name=Our+generator'
            '&area=1.22&Sisc=33&Svoc=55&Smpp=852&Simp=14&Svmp=-0.001&Sff=41&Nisc=-0.001&Nvoc=0'
            '.141&Nmpp=14&Nimp=1444&Nvmp=444&Nff=44&iscTemp=44&vocTemp=44&mppTemp=44&100w=4401'
            '&200w=44&300w=44&400w=4440.001&500w=44&600w=4444&700w=444&800w=44&900w=44&1000w=444'
            '&1y=99&10y=&25y=50&location=25&addNew=Add+New')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertIsNotNone(response.context['valid'])

    def test_add_existing_generator(self):
        """ Test adding existing generator fails """
        print('Test adding an existing generator fails')
        response = self.client.get(
            '/smartergrid/addNew?csrfmiddlewaretoken'
            '=9OeDCigl0J8kbsJCy6p4mCqtK4H7FhmtZFDC2nqCerkTGtfcDAKmxvgeJtGGKfXq&name=TSM-DE05A-305'
            '&area=1.22&Sisc=33&Svoc=55&Smpp=852&Simp=14&Svmp=-0.001&Sff=41&Nisc=-0.001&Nvoc=0'
            '.141&Nmpp=14&Nimp=1444&Nvmp=444&Nff=44&iscTemp=44&vocTemp=44&mppTemp=44&100w=4401'
            '&200w=44&300w=44&400w=4440.001&500w=44&600w=4444&700w=444&800w=44&900w=44&1000w=444'
            '&1y=99&10y=&25y=50&location=25&addNew=Add+New')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertIsNotNone(response.context['generators'])

    def test_add_new_valid_with_adding_generator_with_10yr(self):
        """ Test that adding a generator works """
        print('Test adding a new generator with 10 year warranty')
        response = self.client.get(
            '/smartergrid/addNew?csrfmiddlewaretoken'
            '=9OeDCigl0J8kbsJCy6p4mCqtK4H7FhmtZFDC2nqCerkTGtfcDAKmxvgeJtGGKfXq&name=Another'
            '+generator&area=1.22&Sisc=33&Svoc=55&Smpp=852&Simp=14&Svmp=-0.001&Sff=41&Nisc=-0.001'
            '&Nvoc=0.141&Nmpp=14&Nimp=1444&Nvmp=444&Nff=44&iscTemp=44&vocTemp=44&mppTemp=44&100w'
            '=4401&200w=44&300w=44&400w=4440.001&500w=44&600w=4444&700w=444&800w=44&900w=44&1000w'
            '=444&1y=99&10y=50&25y=&location=25&addNew=Add+New')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertIsNotNone(response.context['valid'])

    def test_add_new_valid_with_adding_generator_with_special_chars(self):
        """ Test that adding a generator works """
        print('Test adding a new generator with invalid characters(name)')
        response = self.client.get(
            '/smartergrid/addNew?csrfmiddlewaretoken'
            '=9OeDCigl0J8kbsJCy6p4mCqtK4H7FhmtZFDC2nqCerkTGtfcDAKmxvgeJtGGKfXq&name=Our+:enerator'
            '&area=1.22&Sisc=33&Svoc=55&Smpp=852&Simp=14&Svmp=-0.001&Sff=41&Nisc=-0.001&Nvoc=0'
            '.141&Nmpp=14&Nimp=1444&Nvmp=444&Nff=44&iscTemp=44&vocTemp=44&mppTemp=44&100w=4401'
            '&200w=44&300w=44&400w=4440.001&500w=44&600w=4444&700w=444&800w=44&900w=44&1000w=444'
            '&1y=99&10y=&25y=50&location=25&addNew=Add+New')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertEqual(response.context['valid'], False)


class TestUpdateSolar(TestCase):
    """ Class to test update solar panel """
    client = MongoClient()

    @classmethod
    def setUpClass(cls):
        """ Add test generator to database """
        print('Adding test generator to database')
        database = cls.client.smartergridDB
        database.generators.insert_one({"type": "custom",
                                        "gen": "Test", 'details': {"AREA": 1.6616,
                                                                   "STC": {"Isc": 9.45,
                                                                           "Voc": 38.8,
                                                                           "Mpp": 285.0,
                                                                           "Imp": 9.08,
                                                                           "Vmp": 31.4,
                                                                           "FF": 77.73},
                                                                   "NOCT": {"Isc": 7.63,
                                                                            "Voc": 35.9,
                                                                            "Mpp": 211.0,
                                                                            "Imp": 7.18,
                                                                            "Vmp": 29.4,
                                                                            "FF": 77.03},
                                                                   "TEMP": {
                                                                       "IscTemp": 0.05,
                                                                       "VocTemp": -0.32,
                                                                       "MppTemp": -0.41},
                                                                   "IRRADEFF": [16.1,
                                                                                16.75,
                                                                                16.8,
                                                                                17.1,
                                                                                17.1,
                                                                                17.25,
                                                                                17.25,
                                                                                17.25,
                                                                                17.24,
                                                                                17.2],
                                                                   "WARRANTEFF": [97.5,
                                                                                  97.5,
                                                                                  96.8,
                                                                                  96.1,
                                                                                  95.4,
                                                                                  94.7,
                                                                                  94,
                                                                                  93.3,
                                                                                  92.6,
                                                                                  91.9,
                                                                                  91.2,
                                                                                  90.5,
                                                                                  89.8,
                                                                                  89.1,
                                                                                  88.4,
                                                                                  87.7,
                                                                                  87,
                                                                                  86.3,
                                                                                  85.6,
                                                                                  84.9,
                                                                                  84.2,
                                                                                  83.5,
                                                                                  82.8,
                                                                                  82.1,
                                                                                  81.4,
                                                                                  80.7]
                                                                   }})

    @classmethod
    def tearDownClass(cls):
        """ Remove test panel from database """
        print('Removing test generator from database')
        database = cls.client.smartergridDB
        database.generators.delete_one({'gen': 'Testing'})
        database.generators.delete_one({'gen': 'Test'})
        cls.client.close()

    def test_update_panel(self):
        """ Test updating panel """
        print('Test updating panel')
        response = self.client.get(
            '/smartergrid/updateSolar?name=Testing&oldName=Test&area=1.6616&Sisc=9.45&Svoc=38.8'
            '&Smpp=285.0&Simp=9.08&Svmp=31.4&Sff=77.73&Nisc=7.63&Nvoc=35.9&Nmpp=211.0&Nimp=7.18'
            '&Nvmp=29.4&Nff=77.03&iscTemp=0.05&vocTemp=-0.32&mppTemp=-0.41&100w=16.1&200w=16.75'
            '&300w=16.8&400w=17.1&500w=17.1&600w=17.25&700w=17.25&800w=17.25&900w=17.24&1000w=17'
            '.2&1y=97.5&10y=91.2&25y=80.7&location=10&add=Update')

        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertContains(response, 'Testing')

    def test_update_panel_with_special_characters(self):
        """ Testing update with special characters """
        print('Test update panel with special characters')
        response = self.client.get(
            '/smartergrid/updateSolar?name=Test;:::&oldName=Testing&area=1.6616&Sisc=9.45&Svoc=38.8'
            '&Smpp=285.0&Simp=9.08&Svmp=31.4&Sff=77.73&Nisc=7.63&Nvoc=35.9&Nmpp=211.0&Nimp=7.18'
            '&Nvmp=29.4&Nff=77.03&iscTemp=0.05&vocTemp=-0.32&mppTemp=-0.41&100w=16.1&200w=16.75'
            '&300w=16.8&400w=17.1&500w=17.1&600w=17.25&700w=17.25&800w=17.25&900w=17.24&1000w=17'
            '.2&1y=97.5&10y=91.2&25y=80.7&location=10&add=Update')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertNotContains(response, 'Test;:::')


class TestDeletePanel(TestCase):
    """ Tests for delete panel """
    client = MongoClient()

    @classmethod
    def setUpClass(cls):
        """ Add test generator to database """
        print('Adding test generator to database')
        database = cls.client.smartergridDB
        database.generators.insert_one({"type": "custom",
                                        "gen": "Test", 'details': {"AREA": 1.6616,
                                                                   "STC": {"Isc": 9.45,
                                                                           "Voc": 38.8,
                                                                           "Mpp": 285.0,
                                                                           "Imp": 9.08,
                                                                           "Vmp": 31.4,
                                                                           "FF": 77.73},
                                                                   "NOCT": {"Isc": 7.63,
                                                                            "Voc": 35.9,
                                                                            "Mpp": 211.0,
                                                                            "Imp": 7.18,
                                                                            "Vmp": 29.4,
                                                                            "FF": 77.03},
                                                                   "TEMP": {
                                                                       "IscTemp": 0.05,
                                                                       "VocTemp": -0.32,
                                                                       "MppTemp": -0.41},
                                                                   "IRRADEFF": [16.1,
                                                                                16.75,
                                                                                16.8,
                                                                                17.1,
                                                                                17.1,
                                                                                17.25,
                                                                                17.25,
                                                                                17.25,
                                                                                17.24,
                                                                                17.2],
                                                                   "WARRANTEFF": [97.5,
                                                                                  97.5,
                                                                                  96.8,
                                                                                  96.1,
                                                                                  95.4,
                                                                                  94.7,
                                                                                  94,
                                                                                  93.3,
                                                                                  92.6,
                                                                                  91.9,
                                                                                  91.2,
                                                                                  90.5,
                                                                                  89.8,
                                                                                  89.1,
                                                                                  88.4,
                                                                                  87.7,
                                                                                  87,
                                                                                  86.3,
                                                                                  85.6,
                                                                                  84.9,
                                                                                  84.2,
                                                                                  83.5,
                                                                                  82.8,
                                                                                  82.1,
                                                                                  81.4,
                                                                                  80.7]
                                                                   }})

    def test_delete_panel(self):
        """ Test deleting a panel """
        print('Test deleting panel')
        response = self.client.get('/smartergrid/deleteSolar/(Test)')

        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertNotContains(response, 'Test')

    @classmethod
    def tearDownClass(cls):
        """ close database client """
        cls.client.close()


class TestValidateFunction(TestCase):

    def test_name(self):
        """ Test name with special characters """
        print('Test name with special characters')
        valid = valitateInputs('gen@', '1.7',
                               {"Isc": 9.45,
                                "Voc": 38.8,
                                "Mpp": 285.0,
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": -0.32,
                                "MppTemp": -0.41},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_area(self):
        """ Test area with special characters """
        print('Test area with special characters')
        valid = valitateInputs('gen', '1@7',
                               {"Isc": 9.45,
                                "Voc": 38.8,
                                "Mpp": 285.0,
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": -0.32,
                                "MppTemp": -0.41},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_stc_isc(self):
        """ Test area with special characters """
        print('Test stc with special characters')
        valid = valitateInputs('gen', '1.7',
                               {"Isc": '9@45',
                                "Voc": 38.8,
                                "Mpp": 285.0,
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": -0.32,
                                "MppTemp": -0.41},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_stc_voc(self):
        """ Test area with special characters """
        print('Test stc with special characters')
        valid = valitateInputs('gen', '1.7',
                               {"Isc": 9.45,
                                "Voc": '38@8',
                                "Mpp": 285.0,
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": -0.32,
                                "MppTemp": -0.41},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_stc_mpp(self):
        """ Test area with special characters """
        print('Test stc with special characters')
        valid = valitateInputs('gen', '1.7',
                               {"Isc": 9.45,
                                "Voc": 38.8,
                                "Mpp": '285@0',
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": -0.32,
                                "MppTemp": -0.41},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_stc_imp(self):
        """ Test area with special characters """
        print('Test stc with special characters')
        valid = valitateInputs('gen', '1.7',
                               {"Isc": 9.45,
                                "Voc": 38.8,
                                "Mpp": 285.0,
                                "Imp": '9/08',
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": -0.32,
                                "MppTemp": -0.41},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_stc_vmp(self):
        """ Test area with special characters """
        print('Test stc with special characters')
        valid = valitateInputs('gen', '1.7',
                               {"Isc": 9.45,
                                "Voc": 38.8,
                                "Mpp": 285.0,
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": '29+4',
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": -0.32,
                                "MppTemp": -0.41},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_stc_ff(self):
        """ Test area with special characters """
        print('Test stc with special characters')
        valid = valitateInputs('gen', '1.7',
                               {"Isc": 9.45,
                                "Voc": 38.8,
                                "Mpp": 285.0,
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": '77.<>3'},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": -0.32,
                                "MppTemp": -0.41},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_stc_isct(self):
        """ Test area with special characters """
        print('Test stc with special characters')
        valid = valitateInputs('gen', '1.7',
                               {"Isc": 9.45,
                                "Voc": 38.8,
                                "Mpp": 285.0,
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": '0@05',
                                "VocTemp": -0.32,
                                "MppTemp": -0.41},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_stc_voct(self):
        """ Test area with special characters """
        print('Test stc with special characters')
        valid = valitateInputs('gen', '1.7',
                               {"Isc": 9.45,
                                "Voc": 38.8,
                                "Mpp": 285.0,
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": '-0.~~32',
                                "MppTemp": -0.41},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_stc_mppt(self):
        """ Test area with special characters """
        print('Test stc with special characters')
        valid = valitateInputs('gen', '1.7',
                               {"Isc": 9.45,
                                "Voc": 38.8,
                                "Mpp": 285.0,
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": -0.32,
                                "MppTemp": '-0.@1'},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_irradeff(self):
        """ Test area with special characters """
        print('Test irradeff with special characters')
        valid = valitateInputs('gen', '1.7',
                               {"Isc": 9.45,
                                "Voc": 38.8,
                                "Mpp": 285.0,
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": -0.32,
                                "MppTemp": -0.41},
                               [':',
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               [97.5,
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)

    def test_validate_warranteff(self):
        """ Test area with special characters """
        print('Test stc with special characters')
        valid = valitateInputs('gen', '1.7',
                               {"Isc": 9.45,
                                "Voc": 38.8,
                                "Mpp": 285.0,
                                "Imp": 9.08,
                                "Vmp": 31.4,
                                "FF": 77.73},
                               {"Isc": 7.63,
                                "Voc": 35.9,
                                "Mpp": 211.0,
                                "Imp": 7.18,
                                "Vmp": 29.4,
                                "FF": 77.03},
                               {"IscTemp": 0.05,
                                "VocTemp": -0.32,
                                "MppTemp": -0.41},
                               [16.1,
                                16.75,
                                16.8,
                                17.1,
                                17.1,
                                17.25,
                                17.25,
                                17.25,
                                17.24,
                                17.2],
                               ['@',
                                97.5,
                                96.8,
                                96.1,
                                95.4,
                                94.7,
                                94,
                                93.3,
                                92.6,
                                91.9,
                                91.2,
                                90.5,
                                89.8,
                                89.1,
                                88.4,
                                87.7,
                                87,
                                86.3,
                                85.6,
                                84.9,
                                84.2,
                                83.5,
                                82.8,
                                82.1,
                                81.4,
                                80.7])
        self.assertEqual(valid, False)


class TestActualView(TestCase):
    """ Class to test the actual view """

    def test_generators_displayed(self):
        """ Test index page gets passed the generators
        correctly
        """
        print("Testing actual page view (generators)")

        response = self.client.get('/smartergrid/actual')
        self.assertEqual(response.status_code, 200, 'Wrong status code from get request.')
        self.assertEqual(response.context['generators'],
                         [["Q.Pro-G3-250", 'testPanelLocation'], ['ALM-250D-Rugby', 'Rugby'],
                          ["SW 285-Mono --Newquay", 'Newquay']])
