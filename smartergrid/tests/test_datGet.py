"""File for testing the weather data collection script"""
import os
import unittest as ut
import datetime
from smartergrid.scripts.datGet import get_keys, get_locs, get_sunrise, get_sunset, owm_request, yahoo_request, convert_kmh_mps, convert_pm_allday, convert_to_kelvin


BASE = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
OWM_DATA = os.path.join(BASE, 'scripts', 'data', 'owmGlasgowTEST.json')
YH_DATA = os.path.join(BASE, 'scripts', 'data', 'yhGlasgowTEST.json')
LOCS_DATA = os.path.join(BASE, 'scripts', 'locs.txt')

class TestGetters(ut.TestCase):
    """ Testing all the getters of the scripts"""


    def test_check_get_keys(self):
        """Checking the get keys function"""
        API_FILES = ["owmKey.txt", "yhKey.txt", "yhOA.txt"]
        key_dict = get_keys()
        #Checking if the dictionary contains anything
        self.assertTrue(bool(key_dict))
        #Checking it contains the right keys
        self.assertTrue(((API_FILES[0] in key_dict) and (API_FILES[1] in key_dict) and (API_FILES[2] in key_dict)))

    def test_check_get_locs(self):
        """Checking the get locations function"""
        locs = get_locs()
        #Check get locs returns the right number of values
        with open(LOCS_DATA) as loc_file:
            real_locs = loc_file.readlines()
            loc_num = len(real_locs)
        self.assertTrue(len(locs) == loc_num)

        #Check the locs data is correctly formatted
        #There should be four data types split up by comma
        self.assertTrue(len(locs[0].split(",")) == 4)

    def check_get_sunrise(self):
        #Check get sunrise returns an time within proper bounds
        keys = get_keys()
        sun_rise = int(get_sunrise(keys))
        self.assertTrue((sun_rise >= 0 and sun_rise <= 24))

    def check_get_sunset(self):
        #Check get sunset returns an time within proper bounds
        keys = get_keys()
        sun_set = int(get_sunset(keys))
        self.assertTrue((sun_set >= 0 and sun_set <= 24))

class TestConverters(ut.TestCase):
    """class for testing all the converters in datGet"""

    def test_check_convert_to_kelvin(self):
        """Checking my kelvin converter"""
        #Check to see if the converter converts 0C into 273.15 kelvin (the correct conversion)
        init_temp = 0
        adjusted_temp = convert_to_kelvin(init_temp)
        correct_temp = 273.15
        self.assertEqual(correct_temp, adjusted_temp)

    def test_check_converting_to_mps(self):
        """Checking to see if the conversion from km/h to m/s works"""
        init_speed = 3.6
        adjusted_speed = convert_kmh_mps(init_speed)
        correct_speed = 1
        self.assertEqual(correct_speed, adjusted_speed)


    def test_check_time_converter(self):
        """Checking converter to pm works correctly"""
        ##1pm = 13th hour
        init_time = 1
        adjusted_time = convert_pm_allday(init_time)
        correct_time = 13
        self.assertEqual(correct_time, adjusted_time)

class TestDataRetrieval(ut.TestCase):
    """This class is for testing the main data getter and storing the data as clean json"""

    @classmethod
    def setUpClass(cls):
        """Delete the files created"""
        if os.path.exists(OWM_DATA):
            os.remove(OWM_DATA)
        
        if os.path.exists(YH_DATA):
            os.remove(YH_DATA)
        
        


    # Class vars
    CORRECT_LINE_NO_OWM = 13
    CORRECT_LINE_NO_YH = 11
    loc_param_string = "GlasgowTEST,55.860346 -4.253742,19,1"
    keys_param = get_keys()
    json_element = '  "'+str(datetime.datetime.now().date())+'": {\n'

    def test_test_owm_getter(self):
        # Calls the function to get the data and save it, SHould ret 1
        ret_val_owm = owm_request(self.loc_param_string, self.keys_param)
        self.assertEqual(ret_val_owm, 1)


    def test_test_yh_getter(self):
        # Calls the function to get the data and save it, SHould ret 1
        ret_val_yh = yahoo_request(self.loc_param_string, self.keys_param)
        self.assertEqual(ret_val_yh, 1)


    #The functions also call the create file function. So as a result there should be some new files in the
    #Data folder which contain the weather data

class TestNewGetters(ut.TestCase):

    # Class vars
    CORRECT_LINE_NO_OWM = 13
    CORRECT_LINE_NO_YH = 11
    loc_param_string = "GlasgowTEST,55.860346 -4.253742,19,1"
    keys_param = get_keys()
    json_element = '  "'+str(datetime.datetime.now().date())+'": {\n'

    def test_check_file_creation(self):
        """Checking the files got created successfully"""
        #check the file has been created
        self.assertTrue(os.path.exists(OWM_DATA))
        self.assertTrue(os.path.exists(YH_DATA))

    def test_check_owm_file_formatting(self):
        """Testing the formatting the owm api file"""
        #Load in the files
        with open(OWM_DATA) as owm_file:
            owm_lines = owm_file.readlines()

        #check that the correct number of lines is there
        self.assertEqual(len(owm_lines), self.CORRECT_LINE_NO_OWM)

        #checks the json file is of the correct structure and is at the time
        self.assertEqual(self.json_element, owm_lines[1])

    def test_test_yahoo_file_formatting(self):
        """Testing the formatting the yahoo api file"""
        # load the file
        with open(YH_DATA) as yh_file:
            yh_lines = yh_file.readlines()
        # check the correct number of lines is there
        self.assertEqual(len(yh_lines), self.CORRECT_LINE_NO_YH)

        # check the json file is formatted correctly
        self.assertEqual(self.json_element, yh_lines[1])

    @classmethod
    def tearDownClass(cls):
        """Delete the files created"""
        os.remove(OWM_DATA)
        os.remove(YH_DATA)