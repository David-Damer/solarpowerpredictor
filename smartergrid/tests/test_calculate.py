"""File for testing the calculate function"""
import os.path
import pickle
import unittest
from datetime import timedelta, datetime

import numpy as np

from smartergrid.calculate import calculate

BASE = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PATH = os.path.join(BASE, 'tests', 'test_data')

CUM_EX = np.loadtxt(os.path.join(PATH, 'test_data_cumulative.txt'))
TOT_EX = np.loadtxt(os.path.join(PATH, 'test_data_total.txt'))
GEN = {'gen': 'TSM-DE05A-305', 'details': {'AREA': 1.6368,
                                           'STC': {'Isc': 9.85, 'Voc': 40.0, 'Mpp': 0.305,
                                                   'Imp': 9.28, 'Vmp': 32.9,
                                                   'FF': 77.49},
                                           'NOCT': {'Isc': 7.95, 'Voc': 37.2, 'Mpp': 0.227,
                                                    'Imp': 7.46, 'Vmp': 30.5,
                                                    'FF': 76.93},
                                           'TEMP': {'IscTemp': 0.05, 'VocTemp': -0.29,
                                                    'MppTemp': -0.39},
                                           'IRRADEFF': [17.08, 17.09, 17.12, 17.15, 17.21,
                                                        17.25, 17.51, 18.09, 18.53, 18.6],
                                           'WARRANTEFF': [97, 97, 96.22, 95.44, 94.66,
                                                          93.88, 93.1, 92.32, 91.54, 90.76,
                                                          90.0, 89.22, 88.45, 87.67,
                                                          86.9, 86.12, 85.35, 84.57,
                                                          83.8, 83.03, 82.26, 81.49,
                                                          80.72, 79.95, 79.18, 78.41]}}

with open(os.path.join(PATH, 'test_data_weather.pkl'), 'rb') as weather:
    WEATHER = pickle.load(weather)

DATE = '2018-12-05'
DATE_JAN = '2019-01-01'
WEATHER_JAN = {DATE_JAN: WEATHER[DATE]}


class TestCalculate(unittest.TestCase):
    """Class to test the calculate function"""

    def test_calculate_against_expected(self):
        """Testing against expected values"""
        print("Testing Calculate against expected values")
        total, cumulative = calculate(100, GEN, 1.0, DATE + 'T00:00:00', DATE + 'T23:59:59',
                                      float(55.860346),
                                      float(-4.253742), 0.0, 0.0, 0.0, WEATHER)
        for i, item in enumerate(total):
            self.assertAlmostEqual(item*10, TOT_EX[i], places=7, msg='totals are not equal ' + str(i))
            self.assertAlmostEqual(cumulative[i]*10, CUM_EX[i], places=7,
                                   msg='cumulative is not equal')

    def test_calculate_with_no_generator(self):
        """Testing a KeyError is thrown when
        no generator is passed to calculate
        """
        print("Testing Calculate with no generator")
        with self.assertRaises(KeyError):
            calculate(100, {}, 1, DATE + 'T00:00:00', DATE + 'T23:59:59',
                      float(55.860346),
                      float(-4.253742), 0.0, 0.0, 0.0, WEATHER)

    def test_calculate_with_no_weather(self):
        """Testing TypeError is thrown when
        no weather is passed to calculate
        """
        print("Testing Calculate with no weather")
        with self.assertRaises(TypeError):
            calculate(100, GEN, 1, DATE + 'T00:00:00', DATE + 'T23:59:59',
                      float(55.860346),
                      float(-4.253742), 0.0, 0.0, 0.0, {})

    def test_calculate_with_no_date(self):
        """Testing ValueError is thrown when
        no date is passed to calculate
        """
        print("Testing Calculate with no date")
        with self.assertRaises(ValueError):
            calculate(100, GEN, 1, 'T00:00:00', 'T23:59:59', float(55.860346),
                      float(-4.253742), 0.0, 0.0, 0.0, WEATHER)

    def test_calculate_with_lat_lon_as_string(self):
        """Testing TypeError is thrown when
        lat and lon is passed to calculate
        as strings
        """
        print("Testing Calculate with lat/lon as string")
        with self.assertRaises(TypeError):
            calculate(100, GEN, 1, DATE + 'T00:00:00', DATE + 'T23:59:59',
                      str(55.860346),
                      str(-4.253742), 0.0, 0.0, 0.0, WEATHER)

    def test_calculate_with_future_date(self):
        """Testing TypeError is thrown when
        future date is passed to calculate
        """
        print("Testing Calculate with future date")
        diff = timedelta(days=1)
        date = datetime.today() + diff
        with self.assertRaises(TypeError):
            calculate(100, GEN, 1, date + 'T00:00:00', date + 'T23:59:59',
                      55.860346,
                      -4.253742, 0.0, 0.0, 0.0, WEATHER)

    def test_calculate_with_low_month(self):
        """Testing calculate works with month < 10"""
        print("Testing calculate with month < 10")
        total, cumulative = calculate(100, GEN, 1, DATE_JAN + 'T00:00:00', DATE_JAN + 'T23:59:59',
                                      float(55.860346),
                                      float(-4.253742), 0.0, 0.0, 0.0, WEATHER_JAN)
        self.assertIsNot((total, cumulative), (None, None))
