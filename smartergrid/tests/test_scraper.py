"""File for testing the scraper function which collects real weather data and puts
   it into a three column file"""

import datetime
import os
import unittest as ut

from smartergrid.scripts.scraper import Scraper

BASE = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TPL_DATA = os.path.join(BASE, 'scripts', 'data', 'ActualTestPanelLocTEST.json')
NQG_DATA = os.path.join(BASE, 'scripts', 'data', 'ActualNewquayGenTEST.json')
RBG_DATA = os.path.join(BASE, 'scripts', 'data', 'ActualRugbyGenTEST.json')
DATA_FILES = [TPL_DATA, NQG_DATA, RBG_DATA]
DICTLENGTH = 3
DATAPOINTLEN = 3

class TestCreation(ut.TestCase):
    """Create an instance of the scraper and check its attributes are set correctly"""



    scp = Scraper(
        {"ActualTestPanelLocTEST": "https://pvoutput.org/intraday.jsp?id=6438&sid=26965&dt=",
         "ActualNewquayGenTEST": "https://pvoutput.org/intraday.jsp?id=66660&sid=59275&dt=",
         "ActualRugbyGenTEST": "https://pvoutput.org/intraday.jsp?id=8191&sid=6633&dt="})

    def test_save_data(self):
        """check the save data is empty on creation of the scraper"""
        self.assertFalse(self.scp.save_data)

    def test_urls(self):
        """Check the urls got loaded properly"""
        self.assertEqual(len(self.scp.urls), DICTLENGTH)


class TestMethods(ut.TestCase):
    """Testing the scrapers methods"""

    init_element = '{\n'
    json_element = '  "' + str(datetime.datetime.now().date()) + '": [\n'
    scp = Scraper(
        {"ActualTestPanelLocTEST": "https://pvoutput.org/intraday.jsp?id=6438&sid=26965&dt=",
         "ActualNewquayGenTEST": "https://pvoutput.org/intraday.jsp?id=66660&sid=59275&dt=",
         "ActualRugbyGenTEST": "https://pvoutput.org/intraday.jsp?id=8191&sid=6633&dt="})

    @classmethod
    def setUpClass(cls):
        cls.scp.get_today()
        cls.scp.save_today()

    def test_get_today(self):
        """Testing the get today method of the scraper"""

        # Check it populated the dict with the corrent number of entries
        self.assertTrue(self.scp.save_data)

        # if the time is passed 6:25 am then the generation curves will have started
        # being plotted which means the first generator should have data we can
        # check the formatting of
        time = str(datetime.datetime.now().time())
        if int(time.split(":")[0]) > 6 or (
                int(time.split(":")[1]) > 35 and int(time.split(":")[0]) == 6):
            self.assertEqual(len(self.scp.save_data), DATAPOINTLEN)

    def test_save_data(self):
        """Test the function which saves the data to the file"""

        for f in DATA_FILES:
            # check the files exist
            self.assertTrue(os.path.exists(f))

            # Check for formatting is correct
            with open(f) as indiv_file:
                data_points = indiv_file.readlines()
                # Contains the proper date
                self.assertEqual(data_points[1], self.json_element)

                # has correct json formatting
                self.assertEqual(data_points[0], self.init_element)

    @classmethod
    def tearDownClass(cls):
        """Delete the files created"""
        os.remove(TPL_DATA)
        os.remove(NQG_DATA)
        os.remove(RBG_DATA)
