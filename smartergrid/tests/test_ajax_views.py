""" File for the ajax views tests """
from django.test import TestCase
from pymongo import MongoClient

from smartergrid.ajax_views import get_location_parameters, read_from_database


class TestGetLocation(TestCase):
    """ Class to test get_location_parameters function """

    def test_get_location_parameters(self):
        """ Testing correct parameters are returned """
        print('Testing get location parameters')

        lat, lon, ele, ngens = get_location_parameters('Glasgow')
        self.assertEqual((lat, lon, ele, ngens), ('55.860346', '-4.253742', '19', '1\n'))

    def test_get_location_returns_none(self):
        """ Test that get location returns None with incorrect location """
        print('Testing get location parameters with incorrect location')
        lat, lon, ele, ngens = get_location_parameters('Scarborough')
        self.assertEqual((lat, lon, ele, ngens), (None, None, None, None))


class TestReadFromDatabase(TestCase):
    """ Class to test read_from_database """
    expected_gen = expected_weather = None

    @classmethod
    def setUpClass(cls):
        """ Get expected values from database """
        mclient = MongoClient()
        database = mclient.smartergridDB
        cls.expected_gen = database.generators.find_one({'gen': 'TSM-DE05A-305'})
        cls.expected_weather = database.owmglasgow.find_one({}, {'2018-12-05'})
        del cls.expected_gen['_id']
        del cls.expected_weather['_id']
        mclient.close()

    def test_read_from_database(self):
        """ Test actual against expected """
        print('Test read from database')
        generator, weather = read_from_database('Glasgow', '2018-12-05', 'TSM-DE05A-305', 'owm')
        self.assertEqual((self.expected_gen, self.expected_weather), (generator, weather))


class TestGraphView(TestCase):
    """ Class to test he graph view """

    def test_graph_view(self):
        """ Test graph view with one set of data """
        print('Test graph view with one set of data')
        response = self.client.get(
            '/smartergrid/graph/?day=2019-02-08&gen=Q.Pro-G3-250&loc=Glasgow&comp=0')
        self.assertContains(response,
                            '<script src="/static/js/chart.js">')

    # def test_graph_view_with_both(self):
    #     """ Test graph view with 2 sets of data """
    #     print('Test graph view with 2 sets of data')
    #     response = self.client.get(
    #         '/smartergrid/graph/?day=2019-02-10&gen=JKM315M-60H&loc=Brighton&dayC=2019-02-10&genC'
    #         '=Q.Pro-G3-250&locC=Brighton&comp=1')
    #     self.assertContains(response,
    #                         '<script src="/static/js/chart.js">')

    def test_graph_view_with_malformed_get(self):
        """ Tests graph view with malformed get request """
        print('Test graph view with malformed get request')
        response = self.client.get('/smartergrid/graph/')
        self.assertRedirects(response, '/smartergrid/', status_code=302)

    def test_graph_view_with_post(self):
        """ Tests graph view with post request """
        print('Test graph view with post request')
        response = self.client.post('/smartergrid/graph/')
        self.assertRedirects(response, '/smartergrid/', status_code=302)


class TestCompareActualView(TestCase):
    """ Class to test compare actual view """

    def test_compare_view_with_malformed_get(self):
        """ Tests compare view with malformed get request """
        print('Test compare view with malformed get request')
        response = self.client.get('/smartergrid/compare/')
        self.assertRedirects(response, '/smartergrid/', status_code=302)

    def test_compare_view_with_post(self):
        """ Tests compare view with post request """
        print('Test graph view with post request')
        response = self.client.post('/smartergrid/compare/')
        self.assertRedirects(response, '/smartergrid/', status_code=302)

    def test_compare_context(self):
        """ Test returned context """
        print('Test compare returns correct context')
        response = self.client.get(
            '/smartergrid/compare/?day=2019-02-18&gen=SW 285-Mono --Newquay&loc=Newquay')
        self.assertIsNotNone(response.context['json_compare'])
