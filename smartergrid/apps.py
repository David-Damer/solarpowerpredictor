from django.apps import AppConfig


class SmartergridConfig(AppConfig):
    name = 'smartergrid'
