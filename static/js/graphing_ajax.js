var compare = 0;
$("#comp").click(function () {
    compare = compare + 1;
    console.log(compare);
});

$("#graphData").submit(function (e) {
    e.preventDefault();
    var values = $(this).serializeArray();
    var day = values[1].value;
    var location = values[3].value;
    var generator = values[2].value;
    var dayC = values[4].value;
    var locationC = values[6].value;
    var generatorC = values[5].value;
    if (compare % 2 === 0) {
        console.log("selected=", day, location, generator);
        if (day === '' || location === 'Choose...' || generator === 'Choose...') {
            alert('Please enter all required details!');
        } else {
            $.get('/smartergrid/graph', {
                day: day,
                gen: generator,
                loc: location,
                comp: compare
            }, function (data) {
                $('#graph').html(data);
            });
        }
    } else {
        console.log("selected=", day, location, generator, dayC, locationC, generatorC);
        if (day === '' || location === 'Choose...' || generator === 'Choose...' || dayC === '' || locationC === 'Choose...' || generatorC === 'Choose...') {
            alert('Please enter all required details!');
        } else {
            $.get('/smartergrid/graph', {
                day: day,
                gen: generator,
                loc: location,
                dayC: dayC,
                genC: generatorC,
                locC: locationC,
                comp: compare
            }, function (data) {
                $('#graph').html(data);
            });
        }
    }
});

