$("#graphData2").submit(function (e) {
    e.preventDefault();
    var values = $(this).serializeArray();
    console.log(values);
    var day = values[1].value;
    var gendata = JSON.parse(values[2].value.replace(/'/g, "\""));
    var gen = gendata[0];
    var loc = gendata[1];
    console.log(gen, loc);

    $.get('/smartergrid/compare', {
        day: day,
        gen: gen,
        loc: loc,
    }, function (data) {
        $('#compareGraph').html(data);
    });
});

var date2 = document.getElementById('date2');
var gen = document.getElementById('gen');
var generate2 = document.getElementById('generate2');
var sidebar = document.getElementById('model');
var compareGraph = document.getElementById('compareGraph');

try {
    generate2.onclick = function () {
    var info = document.getElementsByClassName("cont");
    for (var i = 0; i < info.length; i++) {
        info[i].style.visibility = "hidden";
        info[i].style.display = "none";
    }
    sidebar.style.position = "absolute";
    sidebar.style.backgroundColor = " #3e4444";
    sidebar.style.width = "20%";
    sidebar.style.height = "100%";
    sidebar.style.marginTop = "0";
    compareGraph.style.marginTop = "100px";
    compareGraph.style.marginLeft = "150px";
    date2.style.width = "80%";
    gen.style.width = "80%";
}} catch (e){
    if (e instanceof TypeError){
        console.log(e);
    }
};
