var checkBox = document.getElementById('comp');
var dateC = document.getElementById('dateC');
var genC = document.getElementById('genC');
var locC = document.getElementById('locC');
var date = document.getElementById('date');
var gen = document.getElementById('gen');
var loc = document.getElementById('loc');
var outputTitle = document.getElementById('h2');
var generate = document.getElementById('generate');
var sidebar = document.getElementById('model');
var graph = document.getElementById('graph');


checkBox.onclick = function () {
    if (checkBox.checked) {
        dateC.style.opacity = 1;
        genC.style.opacity = 1;
        locC.style.opacity = 1;
    } else {
        dateC.style.opacity = 0;
        genC.style.opacity = 0;
        locC.style.opacity = 0;
    }
};

generate.onclick = function () {
    if (gen.value != "Generator..." || genC.value != "Generator..."){
    if (loc.value != "Location..." || locC.value != "Location..."){
    if (date.value != "" || dateC.value != ""){
    var info = document.getElementsByClassName("cont");
    for (var i = 0; i < info.length; i++) {
        info[i].style.visibility = "hidden";
        info[i].style.display = "none";
    }
    sidebar.style.position = "absolute";
    sidebar.style.backgroundColor = " #3e4444";
    sidebar.style.width = "20%";
    sidebar.style.height = "100%";
    sidebar.style.marginTop = "0";
    graph.style.marginTop = "100px";
    graph.style.marginLeft = "150px";
    dateC.style.marginTop = "20px";
    genC.style.marginTop = "20px";
    locC.style.marginTop = "20px";
    dateC.style.width = "80%";
    genC.style.width = "80%";
    locC.style.width = "80%";
    date.style.width = "80%";
    gen.style.width = "80%";
    loc.style.width = "80%";
}}}
};
