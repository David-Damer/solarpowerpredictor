function create_error_bars(label_array, std_dev) {
    "use strict";
    var error_bars = [];
    for (var i = 0; i < label_array.length; i++) {
        error_bars[i] = std_dev;

    }
    return error_bars;

}


var ctx = $("#compareChart");
var data = $("#json-compare-dd").attr("data-vals");
data = JSON.parse(data);
var actual_total = data.actual_total;
var actual_cumulative = data.actual_cumulative;
var actual_times = data.actual_times;
var total_owm = data.total_owm;
var cumulative_owm = data.cumulative_owm;
var total_yh = data.total_yh;
var cumulative_yh = data.cumulative_yh;
var err_owm = data.error_owm;
var err_yh = data.error_yh;
var loc = data.loc;
var date = data.date;
var gen = data.gen;
var owm_error = create_error_bars(actual_times, err_owm);
var yh_error = create_error_bars(actual_times, err_yh);
// create an array of zeros the same length as the error bars for hiding
var empty_error = [];
for (var i = 0; i < owm_error.length; i++ ){
    empty_error[i] = 0;
}
console.log(total_owm, yh_error);
var max_cum = Math.max.apply(null, actual_cumulative.concat(cumulative_owm).concat(cumulative_yh));
var max_tot = Math.max.apply(null, actual_total.concat(total_owm).concat(total_yh));
console.log(max_cum, max_tot);
var total = {
    label: "Actual Power ",
    yAxisID: "actual_tot",
    fill: false,
    backgroundColor: 'rgb(255, 0, 0)',
    borderColor: 'rgb(255, 0, 0)',
    data: actual_total,

};
var cumulative = {
    label: "Actual Energy ",
    yAxisID: "actual_cum",
    fill: false,
    backgroundColor: 'rgb(255, 0, 0)',
    borderColor: 'rgb(255, 0, 0)',
    borderDash: [1, 1],
    data: actual_cumulative,
};
var total_owm_data = {
    label: " Power(OWM) ",
    yAxisID: "actual_tot",
    fill: false,
    backgroundColor: 'rgb(0, 0, 255)',
    borderColor: 'rgb(0, 0, 255)',
    data: total_owm,

};
var cumulative_owm_data = {
    label: " Cumulative(OWM) ",
    yAxisID: "actual_cum",
    fill: false,
    backgroundColor: 'rgb(255,0,255)',
    borderColor: 'rgb(0,0,255)',
    borderDash: [1, 1],
    data: cumulative_owm,
    error: owm_error,
};
var total_yh_data = {
    label: " Power(YH) ",
    yAxisID: "actual_tot",
    fill: false,
    backgroundColor: 'rgb(0, 255, 0)',
    borderColor: 'rgb(0, 255, 0)',
    data: total_yh,

};
var cumulative_yh_data = {
    label: " Cumulative(YH) ",
    yAxisID: "actual_cum",
    fill: false,
    backgroundColor: 'rgb(255,0,255)',
    borderColor: 'rgb(0,155,255)',
    borderDash: [1, 1],
    data: cumulative_yh,
    error: yh_error,
};
var datasets = [];
datasets.push(total);
datasets.push(cumulative);
datasets.push(total_owm_data);
datasets.push(cumulative_owm_data);
datasets.push(total_yh_data);
datasets.push(cumulative_yh_data);

// set defaults and globals for graph
Chart.defaults.global.elements.point.radius = 0.2;
Chart.defaults.global.elements.point.hitRadius = 3;
var my_chart = new Chart(ctx, {
        type: 'lineError',
        // The data for our dataset
        data: {
            labels: actual_times,
            datasets: datasets,
        },
        options: {
            title: {
                display: true,
                text: date + " " + loc + " Actual and Predicted " + gen,
                padding: 10,
                fontSize: 20,
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 15,
                }
            },
            maintainAspectRatio: false,
            scales:
                {

                    yAxes: [{
                        gridLines: {
                            display: false,
                        },
                        id: "actual_tot",
                        type: 'linear',
                        position: 'left',
                        scaleLabel: {
                            display: true,
                            labelString: "Power(w)",
                            fontSize: 18,
                        },
                        ticks: {
                            beginAtZero: true,
                            min: 0,
                            max: max_tot*1.15
                        },


                    }, {
                        gridLines: {
                            display: false,
                        },
                        id: "actual_cum",
                        type: 'linear',
                        position: 'right',
                        scaleLabel: {
                            display: true,
                            labelString: "Energy(kwh)",
                            fontSize: 18,
                        },
                        ticks: {
                            beginAtZero: true,
                            min: 0,
                            max: max_cum*1.15,
                        }
                    }],
                    xAxes:
                        [{
                            gridLines: {
                                display: false,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "Time(h)",
                                fontSize: 18,
                            },
                            ticks: {
                                autoSkip: true,
                                autoSkipPadding: 10,
                                beginAtZero: true,
                                min: 0,
                            }
                        }]
                },

        }
    })
;
$('#show-hide-owm').click(function () {
    "use strict";
    if ($(this).val() === "Hide OWM error"){
        $(this).val("Show OWM error");
        cumulative_owm_data.error = empty_error;

    } else {
        $(this).val("Hide OWM error");
        cumulative_owm_data.error = owm_error;
    }

    my_chart.update();

});
$('#show-hide-yh').click(function () {
    "use strict";
    if ($(this).val() === "Hide YH error"){
        $(this).val("Show YH error");
        cumulative_yh_data.error = empty_error;

    } else {
        $(this).val("Hide YH error");
        cumulative_yh_data.error = yh_error;
    }

    my_chart.update();

});