// graph generation and csv output scripts

var TIME_INTERVAL = 1;
var TOTAL_HOURS = 24;
var TOTAL_MINUTES = 60;

function buildTimeArray() {
    // build an array of times for x-axis
    "use strict";
    var time_array = [], str_hour, str_minute, hour, minute, str_time;
    for (hour = 0; hour < TOTAL_HOURS; hour += 1) {
        for (minute = 0; minute < TOTAL_MINUTES; minute += TIME_INTERVAL) {
            if (hour < 10) {
                str_hour = "0" + hour;
            } else {
                str_hour = hour;
            }
            if (minute < 10) {
                str_minute = "0" + minute;
            } else {
                str_minute = minute;
            }
            str_time = str_hour + ":" + str_minute;
            time_array.push(str_time);
        }
    }
    return time_array;
}
// function for use in finding first and last
// non-zero elements of arrays
function greater_than_zero(element) {
    "use strict";
    return element > 0;
}
var OFFSET = 30; // extra time on either side of the graph
// get data from the html
var ctx = $("#myChart");
var data = $("#json-data-dd").attr("data-vals");
data = JSON.parse(data);
console.log(data);
var total_1_owm = data.total_1_owm;
var cumulative_1_owm = data.cumulative_owm;
var total_1_yh = data.total_1_yh;
var cumulative_1_yh = data.cumulative_yh;
var location_1 = data.loc;
var date = data.date;
var generator = data.gen;
var comp = parseInt(data.comp);
var toggle_comparison = comp % 2;
if (toggle_comparison === 1) {
    var location_2 = data.locC;
    var date_2 = data.dateC;
    var generator_2 = data.genC;
    var total_2_owm = data.total2_owm;
    var cumulative_2_owm = data.cumulative2_owm;
    var total_2_yh = data.total2_yh;
    var cumulative_2_yh = data.cumulative2_yh;
    var start_2 = total_2_owm.findIndex(greater_than_zero) - OFFSET;
    var end_2 = (total_2_owm.length - total_2_owm.reverse().findIndex(greater_than_zero)) + OFFSET;
}

// find the first and last non-zero entries in the arrays
// for only displaying sunlight hours on graph
// does not affect csv output
var start = total_1_owm.findIndex(greater_than_zero) - OFFSET;
var end = (total_1_owm.length - total_1_owm.reverse().findIndex(greater_than_zero)) + OFFSET;
if (toggle_comparison === 1) {
    start = Math.min(start, start_2);
    end = Math.max(end, end_2);
}
console.log(start, end);
var time_array = buildTimeArray();

// create graph data sets
var total_1_graph_owm = {
    label: "(OWM) Power " + location_1 + " " + generator + " " + date,
    yAxisID: "total_1_owm-1",
    fill: false,
    backgroundColor: 'rgb(255, 0, 0)',
    borderColor: 'rgb(255, 0, 0)',
    data: total_1_owm.slice(start, end),
};
var total_1_graph_yh = {
    label: "(YH) Power " + location_1 + " " + generator + " " + date,
    yAxisID: "total_1_owm-1",
    fill: false,
    backgroundColor: 'rgb(255, 0, 150)',
    borderColor: 'rgb(255, 0, 150)',
    data: total_1_yh.slice(start, end),
};
var cumulative_1_graph_owm = {
    label: "(OWM) Energy " + location_1 + " " + generator + " " + date,
    yAxisID: "cum-1",
    fill: false,
    backgroundColor: "rgb(255, 255, 255)",
    borderColor: "rgb(255, 0, 0)",
    borderDash: [10, 15],
    data: cumulative_1_owm.slice(start, end),
};
var cumulative_1_graph_yh = {
    label: "(YH) Energy " + location_1 + " " + generator + " " + date,
    yAxisID: "cum-1",
    fill: false,
    backgroundColor: "rgb(255, 255, 255)",
    borderColor: "rgb(255, 0, 150)",
    borderDash: [10, 15],
    data: cumulative_1_yh.slice(start, end),
};
var my_datasets = []; // array to hold our graph definitions
my_datasets.push(total_1_graph_owm);
my_datasets.push(cumulative_1_graph_owm);
my_datasets.push(total_1_graph_yh);
my_datasets.push(cumulative_1_graph_yh);
if (toggle_comparison === 1) {
    // create entries for comparison graph
    var total_2_graph_owm = {
        label: "(OWM) Power " + location_2 + " " + generator_2 + " " + date_2,
        yAxisID: "total_1_owm-1",
        fill: false,
        backgroundColor: 'rgb(0,0,255)',
        borderColor: 'rgb(0,0,255)',
        data: total_2_owm.slice(start, end),
    };
    var total_2_graph_yh = {
        label: "(YH) Power " + location_2 + " " + generator_2 + " " + date_2,
        yAxisID: "total_1_owm-1",
        fill: false,
        backgroundColor: 'rgb(0,155,255)',
        borderColor: 'rgb(0,155,255)',
        data: total_2_yh.slice(start, end),
    };
    var cumulative_2_graph_owm = {
        label: "(OWM) Energy " + location_2 + " " + generator_2 + " " + date_2,
        yAxisID: "cum-1",
        fill: false,
        backgroundColor: 'rgb(255,255,255)',
        borderColor: 'rgb(0,0,255)',
        borderDash: [10, 15],
        data: cumulative_2_owm.slice(start, end),

    };
        var cumulative_2_graph_yh = {
        label: "(YH) Energy " + location_2 + " " + generator_2 + " " + date_2,
        yAxisID: "cum-1",
        fill: false,
        backgroundColor: 'rgb(255,255,255)',
        borderColor: 'rgb(0,155,255)',
        borderDash: [10, 15],
        data: cumulative_2_yh.slice(start, end),

    };
    my_datasets.push(total_2_graph_owm);
    my_datasets.push(cumulative_2_graph_yh);
    my_datasets.push(total_2_graph_yh);
    my_datasets.push(cumulative_2_graph_owm);
}
console.log(my_datasets);
// Get maximum values for y-axes
var MAX_TOTAL;
if (toggle_comparison === 0) {
    MAX_TOTAL = Math.max.apply(null, total_1_owm.concat(total_1_yh));
} else {
    MAX_TOTAL = Math.max.apply(null, total_1_owm.concat(total_2_owm).concat(total_1_yh).concat(total_2_yh));
}
var MAX_CUMULATIVE;
if (toggle_comparison === 0) {
    MAX_CUMULATIVE = Math.max.apply(null, cumulative_1_owm.concat(cumulative_1_yh));
} else {
    MAX_CUMULATIVE = Math.max.apply(null, cumulative_1_owm.concat(cumulative_2_owm).concat(cumulative_1_yh).concat(cumulative_2_yh));
}

// create the graphs title
var title;
if (toggle_comparison === 0) {
    title = location_1 + " " + generator + " " + date;
} else {
    title = location_1 + " " + generator + " " + date + " Vs " + location_2 + " " + generator_2 + " " + date_2;
}
// set defaults and globals for graph
Chart.defaults.global.elements.point.radius = 0.2;
Chart.defaults.global.elements.point.hitRadius = 3;
// create graph using our datasets array
var my_chart = new Chart(ctx, {
        type: 'line',
        // The data for our dataset
        data: {
            labels: time_array.slice(start, end), // labels for x-axis
            datasets: my_datasets,
        },
        options: {
            title: {
                display: true,
                text: title,
                padding: 10,
                fontSize: 20,
            },
            layout: {
                padding: {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 15,
                }
            },
            maintainAspectRatio: false,
            scales:
                {

                    yAxes: [{
                        gridLines: {
                            display: false,
                        },
                        id: "total_1_owm-1",
                        type: 'linear',
                        position: 'left',
                        scaleLabel: {
                            display: true,
                            labelString: "Power(w)",
                            fontSize: 18,
                        },
                        ticks: {
                            beginAtZero: true,
                            min: 0,
                            max: MAX_TOTAL*1.15,
                        },


                    }, {
                        gridLines: {
                            display: false,
                        },
                        id: "cum-1",
                        type: 'linear',
                        position: 'right',
                        scaleLabel: {
                            display: true,
                            labelString: "Energy(kwh)",
                            fontSize: 18,
                        },
                        ticks: {
                            beginAtZero: true,
                            min: 0,
                            max: MAX_CUMULATIVE*1.15,
                        }
                    }],
                    xAxes:
                        [{
                            gridLines: {
                                display: false,
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "Time(h)",
                                fontSize: 18,
                            },
                            ticks: {
                                autoSkip: true,
                                autoSkipPadding: 10,
                                beginAtZero: true,
                                min: 0,
                            }
                        }]
                }
        }
    })
;

// Download csv functionality based on data in the graph
$("#csvButton").click(function (e) {
    console.log("button clicked");
    var csv;
    if (toggle_comparison === 0) {
        csv = 'Time,Power,Energy\n';
    } else {
        csv = 'Time,Power1,Energy1,Power2,Energy2\n';
    }
    for (var i = 0; i < time_array.length; i++) {
        csv += time_array[i] + "," + total_1_owm[i] + "," + cumulative_1_owm[i];
        if (toggle_comparison === 1) {
            csv += "," + total_2_owm[i] + "," + cumulative_2_owm[i];
        }
        csv += '\n';
    }
    console.log(csv);
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    if (toggle_comparison === 1) {
        hiddenElement.download = location_1 + generator + date + location_2 + generator_2 + date_2 + '.csv';
    } else {
        hiddenElement.download = location_1 + generator + date + '.csv';
    }
    hiddenElement.click();
});
