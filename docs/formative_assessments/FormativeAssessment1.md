
# Formative Assessment 1



## Change Management



| Question|Awarded<br>(1-5)|

|:--|--|

|Did the team make small, frequent commits to the project SCM? Are all team members contributing to the SCM?|5|

| Did each commit have an informative log message that had a one sentence summary, explained the rationale for the change in more detail, with a reference to the relevant ticket in the issue management system?|5|

|Is the project SCM appropriately organised according to project/language conventions?  Are there non-configuration items in the repository (e.g. binaries, compiled documentation)?|5|

|Does the team have established practices for managing concurrent development and conflict resolution, such as feature branching?|5|

|Other/discretion||



### Comment



It is impossible to assess the change management carried out by the team at this stage.



The team had met with their customer for the first time on the 17th October. Outcomes for the next meeting on the 24th October, as requested by the customer, were to read up and familiarise themselves with the literature in the project area. As such, there was no discussion on requirements. This discussion will occur on the 24th, after which change management will become much more of a focus.



---



## Project Planning



| Question|Awarded<br>(1-5)|

|:--|--|

|Are the details of ticket (ownership, milestones, issue type, priority etc.) complete in the issue management system?  Are there milestones for each iteration of the project with associated tickets?	|5|

|Are tickets frequently updated during the course of the iteration as tasks are completed, or mostly towards the end of the iteration? Are new tickets created as needed throughout the iteration?	|5|

|Did the team make realistic estimates of the effort required for the iteration (i.e. were all objectives achieved?)  If not, did the team actively manage the project plan and identify low priority tasks to be removed from the iteration?|5|

|Does the project have a wiki page containing key details such as a summary of the project, the locations of issue trackers and SCM systems, and team member contact details and responsibilities?|5|

|Other/discretion||



### Comment



It is impossible to assess the project planning carried out by the team at this stage.



As aforementioned, a discussion on requirements for the project will be carried out on the 24th October. Once complete, more in-depth planning can take place.



---