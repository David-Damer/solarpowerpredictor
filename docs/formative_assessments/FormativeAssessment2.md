# Formative Assessment 2

## Change Management

| Question|Awarded<br>(1-5)|
|:--|--|
|Did the team make small, frequent commits to the project SCM? Are all team members contributing to the SCM?|4|
| Did each commit have an informative log message that had a one sentence summary, explained the rationale for the change in more detail, with a reference to the relevant ticket in the issue management system?|1|
|Is the project SCM appropriately organised according to project/language conventions?  Are there non-configuration items in the repository (e.g. binaries, compiled documentation)?|3|
|Does the team have established practices for managing concurrent development and conflict resolution, such as feature branching?|1|

### Comment
Your commits contain small, incremental changes and most of you appear to be contributing to the SCM. When you make these changes, ensure you entitle your commit messages with short, descriptive titles with a short description describing the changes that are being made with the commit. It becomes difficult for you and your team to succinctly review recent changes when the commit messages are too generic. Perhaps you could consider linking your commits to your tickets via the commit message?

Your directory structure separates the different components of the project well, with folders for different portions of the project. I would suggest against uploading private API keys to your SCM.

You are still committing your changes directly to master. This is bad for a number of reasons. It makes it hard to see which commits are associated with which feature (also making it harder to revert changes), makes it harder to tell whether a commit has been reviewed (making the introduction of bugs far more likely), and means merge conflicts will arise more often due to multiple members modifying the same files at the same time. I strongly suggest employing feature branching instead of committing directly to master.

---

## Project Planning

| Question|Awarded<br>(1-5)|
|:--|--|
|Are the details of ticket (ownership, milestones, issue type, priority etc.) complete in the issue management system?  Are there milestones for each iteration of the project with associated tickets?	|4|
|Are tickets frequently updated during the course of the iteration as tasks are completed, or mostly towards the end of the iteration? Are new tickets created as needed throughout the iteration?	|3|
|Did the team make realistic estimates of the effort required for the iteration (i.e. were all objectives achieved?)  If not, did the team actively manage the project plan and identify low priority tasks to be removed from the iteration?|3|
|Does the project have a wiki page containing key details such as a summary of the project, the locations of issue trackers and SCM systems, and team member contact details and responsibilities?|4|

### Comment
The use of MoSCoW labels for ticket prioritisation, associating tickets with milestones and use of ticket asignees shows that project development towards each milestone has been thought out and planned effectively. Ensure that you fill in all of this information along with informative descriptions for **all** tickets that you create.

At the end of each milestone, ensure that all tickets are updated to reflect their status. For the previous milestone, nearly 50% of your tickets associated with it have not been updated. If a ticket has not been completed, close the ticket if you aren't going to include it in the iteration and describe why it was closed. If you migrate the issue to the next milestone, close the existing ticket and create a new one which references the old for this next milestone.

Good use of the Kanban board for tracking the progress of the ticket over time!

---

## Quality Assurance

| Question|Awarded<br>(1-5)|
|:--|--|
|Was every commit tested in a continuous integration environment by executing an automated regression test suite? |1|
|Do the team immediately fix any broken builds report by the CI environment, or are new features added despite broken builds?|1|
|Do the team perform code reviews?  Has the team tried pair programming to actively review code?|3|
|Is the test suite effective (look for evidence from code coverage and/or mutation testing metrics)?  Does the build/test cycle execute quickly (less than 10 minutes).	|1|

### Comments
There doesn't appear to be any CI or test suite in place to perform automated QA. Try to get this set up as soon as possible.

There is definitely a reviewing process in place. However, as previously mentioned, it is difficult to see if a feature has been reviewed when you are committing directly to master initially. Start performing feature branching and, instead of peer-reviewing every commit to master, peer review merge requests for feature branches.

---

## Process Improvement

| Question|Awarded<br>(1-5)|
|:--|--|
|Has the team elicited and documented software process issues during a retrospective?  Did all team members contribute to the retrospective?|5|
|Have software process problems been thoroughly analysed by the team, e.g. by using a -whys or other root cause analysis technique?	|4|
|Have the team identified process improvement actions?  Have these been documented and assigned to a team member for monitoring/completion?	|4|
|Have results from previous software process improvement actions been evaluated and acted upon?	|5|

### Comments
The retrospective carried out has accurately analysed problems and what went well with the current development process. Perhaps you could have gone a little deeper into finding out these problems were present. However, you have clearly listed activities which will improve the development process.

---