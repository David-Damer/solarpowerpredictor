# Formative Assessment 3

## Change Management

| Question|Awarded<br>(1-5)|
|:--|--|
|Did the team make small, frequent commits to the project SCM? Are all team members contributing to the SCM?|5|
| Did each commit have an informative log message that had a one sentence summary, explained the rationale for the change in more detail, with a reference to the relevant ticket in the issue management system?|4|
|Is the project SCM appropriately organised according to project/language conventions?  Are there non-configuration items in the repository (e.g. binaries, compiled documentation)?|5|
|Does the team have established practices for managing concurrent development and conflict resolution, such as feature branching?|5|

### Comment

You're abiding by a good change management methodology. Well done! Commit log messages are generally informative. However, there's still one or two that simply state "Upload New File". Try to be a bit less generic!

---

## Project Planning

| Question|Awarded<br>(1-5)|
|:--|--|
|Are the details of ticket (ownership, milestones, issue type, priority etc.) complete in the issue management system?  Are there milestones for each iteration of the project with associated tickets?	|5|
|Are tickets frequently updated during the course of the iteration as tasks are completed, or mostly towards the end of the iteration? Are new tickets created as needed throughout the iteration?	|4|
|Did the team make realistic estimates of the effort required for the iteration (i.e. were all objectives achieved?)  If not, did the team actively manage the project plan and identify low priority tasks to be removed from the iteration?|5|
|Does the project have a wiki page containing key details such as a summary of the project, the locations of issue trackers and SCM systems, and team member contact details and responsibilities?|5|

### Comment

It's clear that you're creating detailed tickets that are frequently updated on the Kanban board as they progress. However, there are some tickets which are closed which still have tags indicating that they are still being worked on. Whilst not a huge deal, it is a bit confusing. Try to accurately update all of your tickets as they progress.

---

## Quality Assurance

| Question|Awarded<br>(1-5)|
|:--|--|
|Was every commit tested in a continuous integration environment by executing an automated regression test suite? |5|
|Do the team immediately fix any broken builds report by the CI environment, or are new features added despite broken builds?|5|
|Do the team perform code reviews?  Has the team tried pair programming to actively review code?|4|
|Is the test suite effective (look for evidence from code coverage and/or mutation testing metrics)?  Does the build/test cycle execute quickly (less than 10 minutes).	|1|

### Comments

Good to see CI has been set up for the master branch. There are currently three tests which only confirm that the power calculations work with expected input. You should definitely expand the code coverage of your tests and explore more types of testing (e.g. mutation tests, bounding value tests, unit tests, etc).

---

## Process Improvement

| Question|Awarded<br>(1-5)|
|:--|--|
|Has the team elicited and documented software process issues during a retrospective?  Did all team members contribute to the retrospective?|5|
|Have software process problems been thoroughly analysed by the team, e.g. by using a -whys or other root cause analysis technique?	|5|
|Have the team identified process improvement actions?  Have these been documented and assigned to a team member for monitoring/completion?	|5|
|Have results from previous software process improvement actions been evaluated and acted upon?	|5|

### Comments



---